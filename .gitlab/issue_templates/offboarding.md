## Team Member Offboarding

### Introduction
An overview of the [Gitlab Offboarding Process](https://about.gitlab.com/handbook/people-group/offboarding/) can be found within the handbook. Though the People Experience Team are the Directly Responsible Individuals (DRIs) for team member offboarding there are various stakeholders that play a role in ensuring that the relevant access deprovisioning and system updates take place.

### Time Sensitivities
It is important to note that offboarding issues have a completion due date of **five days** from date of opening i.e. five days from when the team member completes their tenure with GitLab.

This due date excludes access deprovisioning for core systems and tools such as GSuite, Okta, 1Password and Slack which should take place immediately in instances of involuntary termination and at the predetermined time in instances of voluntary termination.

Laptop recovery which falls within the scope of IT Operations may take longer than five days and with this in mind is exempt from the aforementioned due date in instances where a departing team member opts to return their equipment.

### Compliance
Completion of the tasks detailed in the offboarding issue ensures that GitLab remains safeguarded and compliant.  These issues are regularly audited and reviewed making it of the utmost importance that stakeholders indicate when a task has been completed or alternatively when it does not pertain to the departing team member.

### Team Member Particulars

| | |
| --- | --- |
| **Entity** | `__ENTITY__` |
| **Department** | `__DEPARTMENT__` |
| **Manager** | `__MANAGER_HANDLE__` |
| **GitLab Email** | `__GITLAB_EMAIL__` |
| **GitLab.com Handle** |``__GITLAB_HANDLE__``|
| **BAMBOOHR_ID** | `__BAMBOO_ID__` |
| **Hire Date** | `__HIRE_DATE__` |

---

## Offboarding Tasks

<details>
<summary>Manager</summary>

1. [ ] Please comment, tagging the IT Ops team `@gitlab-com/business-technology/team-member-enablement` if you need delegate access to the former Gitlab Team Member's email account.
1. [ ] Please comment, tagging the IT Ops team `@gitlab-com/business-technology/team-member-enablement` if you need the former Gitlab Team member's Google Drive transferred at the time of offboarding.
1. [ ] Manager: Review all open merge requests and issues assigned to the team member and reassign them to you or another team member as per your handover agreement within the team.
1. [ ] Coordinate with IT Ops @gitlab-com/business-technology/team-member-enablement to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if team member had sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
   1. [ ] Review what 1Password vaults former team member had access to, and identify any shared passwords to be changed and moved to Okta.
1. Review the handbook section surrounding [Communicating Departures Company-Wide](https://about.gitlab.com/handbook/people-group/offboarding/#communicating-departures-company-wide) - the nature of the offboarding i.e. voluntary or involuntary will direct when and how news of the departure will be relayed and managers are encouraged to consider the suggestions provided when compiling the announcement.  It is important to take note of [what information can and cannot be shared](https://about.gitlab.com/handbook/people-group/offboarding/#what-do-we-share) surrounding a team members departure prior to compiling the message - if there are areas of uncertainty please be sure to reach out to a People Business Partner for support.
	1. [ ] Upload a screen grab of the departure announcement from within the #team-member-updates channel whether posted by the departing team member (voluntary) or a manager (involuntary) in the comments section of this offboarding issue.
1. [ ] Update Google Calendar events.
   1. [ ] Cancel weekly 1:1 meetings with team member.
   1. [ ] Remove team member from team meeting invitations.
   1. [ ] If applicable, transfer ownership of meetings from team member to a new owner (both in Google Calendar and Zoom).
1. [ ] If applicable, remove team member from [triage-ops notifications](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/group-definition.yml).
1. [ ] Organize a smooth hand over of any work or tasks from the former team member. Offer option to send a message to everyone in the company (i.e. forwarded by the manager), for a farewell message and/or to transmit their personal email address in case people wish to stay in touch.
1. [ ] Please reach out to your [Finance Business Partner ](https://about.gitlab.com/handbook/finance/#finance-business-partner-alignment) to discuss whether you are backfilling this role or not.
1. [ ] Scroll to the bottom of this issue; depending on the department, division, or entity there may be additional manager tasks.

</details>

## People

<details>
<summary>People Experience</summary>

### Complete the below tasks ASAP

1. [ ] People Experience: For this offboarding, the manager is `__MANAGER_HANDLE__`, and People Experience is handled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to them (this should be done automatically, but worth checking).
1. [ ] People Experience: Check that the offboarded team members details are listed correctly in the table above.
1. [ ] People Experience: If the offboarding team member is on the executive team, or if the offboarding team member reports to a member of the executive team, please also assign the issue to the appropriate Executive Business Administrator(https://about.gitlab.com/handbook/eba/).

### BambooHR

1. [ ] The People Business Partner in charge of the offboarding will complete the offboarding [form](https://forms.gle/HzUAVCgTJ4v5HSSA9), which populates the data to the `People Exp/Ops Tracker` as well as in the `#offboarding` slack channel. Confirm all the data required has been completed, including the offboarding Type, offboarding Reason and if the team member is Eligible for Rehire.
1. [ ] Check that BambooHR has been updated with the relevant employment status (including garden leave/lasy working day), termination status and exit impact (as per data in tracker).
1. [ ] **If applicable: Ensure that the addition of the Garden Leave/Last Working Day has not changed the team members hire date (can cause negative impact on compliance and reporting).**
1. [ ] If the team member is on **Garden Leave/Last Working Day**, notify ITOps to manually deactive Okta.
	- _Note: This will also trigger any deprovisioning workflows for Applications that Okta is configured to perform Deprovisioning on, as well as disable the user's Okta account._
1. [ ] Check that if a voluntary separation, that a resignation message has been saved in the Terminations folder. If there is none, contact the People Operations Specialist to see if it was forwarded to `peopleops@gitlab.com`. If not, please contact the manager as this documentation is needed for us to be compliant.
1. [ ] If a team member is terminated before their Probationary Period has completed, delete the "Active" and "End of Probation Period" line dated in the future in the Employment Status section of BambooHR. The top entry of the Employment Status Table should be the "Termination" entry.
1. [ ] Check to see if the team member has any pending bonus payments. If there is a pending payment, reach out to Total Rewards and the PBP to ensure this bonus is paid.
1. [ ] Check to see whether the team member was paid a signing or sign-on bonus. If yes, send an email to Payroll providing them with the team members name, bonus amount and termination date and that the amount needs to be deducted off the team members final pay. Reach out to a People Business Partner to see whether refund is needed if the team member has been with GitLab for more than 12 months.
1. [ ] Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. To do this, log into the [Okta dashboard](https://gitlab-admin.okta.com/) and select `Admin` in the upper-right hand side of the page. Once on the Okta Admin dashboard, select `Applications`, search and select `BambooHR Admin`, go to the `Import` tab and click the `Import Now` button. This will force an import, and you should see a message that a user has been removed after processing completes. (This will not be the case if the team member is placed on Garden Leave).

### Notifications

1. [ ] Notify Security of the offboarding (@JohnathanHunt).
1. [ ] Notify Sr Stock Administrator (@tdominique) of offboarding. If this team member is on garden leave/last working day, please ensure the exact dates and process is shared with the Sr Stock Administrator.
1. [ ] Depending on the team members location, please ping the relevant payroll contact persons in this issue:

         - US and Canada: Vicki Laughlan and Yasmine Basha
         - Non-US: Nicole Precilla and Sylwia Szepietowska

1. [ ] Scroll to the bottom of this issue to ensure there are not additonal People Experience Tasks for Department/Division/Entity/Role Task
1. [ ] Review column AD of the offboarding tracker and please comment in this issue to state:
- `Exit Interview was completed on (date)`  **or**
- `Exit Interview was declined` 			**or**
- `Exit Interview not required`

### Complete the below tasks before due date

### Handbook

1. [ ] Team Page:
    1. [ ] A merge request will be created once the offboarding issue has been created via the Slack pops command. This MR will remove the entries listed below as part of the employment [automation](https://gitlab.com/gitlab-com/people-group/peopleops-eng/employment-automation/-/blob/main/lib/syncing/remove_team_member_from_website.rb).
	1. [ ] Follow the steps listed in the merge request body to remove:
		- [the team page entry and picture](https://about.gitlab.com/team)
		- [pet page](https://about.gitlab.com/team-pets)
		- Remove the team member from CODEOWNERS and replace with manager
1. [ ] If the team member is a people manager or an interim people manager at GitLab, be sure to remove their slug from their direct reports profiles on the team page as well as this will cause the pipeline to fail. If a new manager is not yet assigned, please replace the manager slug with the departing GitLab team member's direct manager.
1. [ ] Check that the team page merge request has been automatically linked to this offboarding issue for ease of reference.
1. [ ] Remove hard-coded reference links of the team member from our documentation and Handbook (i.e. links to the team page) by following the [instructions on the Handbook](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/)
1. [ ] Review and remove any mention of the team member from the [offboarding tasks templates](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/tree/master/.gitlab/issue_templates/offboarding_tasks)
1. [ ] Review and remove any mention of the team member from the [onboarding tasks templates](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/tree/master/.gitlab/issue_templates/onboarding_tasks)


### Access
1. [ ] Delete account in [Moo](https://moo.com).
1. [ ] For both voluntary and involuntary offboardings, confirm the former team member's eligibility to be added to the `gitlab-alumni` Slack channel with the relevant People Partner in the message thread if no confirmation is received in the Slack offboarding form. If the People Partner confirms eligibility, please open an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue to add team member as a single channel guest to `gitlab-alumni`. In the `Person Details` section, only enter the team member's name. Do not add their personal email address, as IT Ops can find it in BambooHR. Please ensure that there is a personal email address listed in BambooHR prior to creating the Access Request issue.

### Slack
1. [ ] Confirm that a departure announcement has been made in #team-member-updates on Slack.
1. [ ] Search for and remove the custom emoji for that team member. The emoji name should be `:firstnamelastname:


</details>

## Business Technology

<details>
<summary>Business Systems Analyst</summary>

1. [ ] Business Systems Analyst (@lisvinueza): Review and confirm if team member is a system admin/provisioner.
    - [ ] Business Systems Analyst (@lisvinueza): If team member is a system admin/provisioner, open and complete an [Update Tech Stack Provisioner issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Update_Tech_Stack_Provisioner).

</details>

<details>
<summary>Team Member Enablement (IT)</summary>

1. [ ] IT Ops: Okta should now deprovision Google Account as part of Okta Deactivation, please verify it was deactivated.
1. [ ] IT Ops: Move the deactivated account into the `Former Team Members` Organizational Unit in G-Suite.
1. [ ] IT Ops: Select the dropdown icon `ˇ` in the `User information` and delete any aliases assigned to the account.
1. [ ] IT Ops: Remove the Former Team Member account from any Google groups, unsuspend the account, clear any sign in cookies and reset the account password. Do not automatically email new password.
1. [ ] IT Ops: Create an Out of Office Message using GAM, pick a related template here if a custom one is not provided. [Offboarding Auto-replies](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/email_templates/offboarding_rejection.md)
1. [ ] IT Ops: As per the Manager's comment in this issue (ping them if they haven't commented yet), please transfer ownership of documents to the Manager, if so requested.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Check if former GitLab Team Member had a gold account on GitLab and if so, downgrade the account.
   - If needed, you can find who has a gold account by searching in the [ARs project](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=GitLab%20Gold%20Request) and the [Gold Entitlement Request project](https://gitlab.com/gitlab-com/team-member-epics/gold-entitlement-request/-/issues?scope=all&utf8=%E2%9C%93&state=all)) for Closed `~"GitLab Gold Request"`s. You can use the `author` filter to narrow it down if needed.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Remove former GitLab Team Member's' GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members).
   1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Block former GitLab Team Member, remove from all company Groups and Projects, and remove GitLab job role if visible on profile.
   1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : **IF** the former GitLab Team Member's start date was **prior to 2020-03-23**, change the primary email address on the GitLab.com account to be a personal email address of the former Team Member and unblock the account.
   1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Remove former GitLabber's admin account, if applicable.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Block former GitLab Team Member's [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Block former GitLab Team Member's [staging.GitLab.com account](https://staging.gitlab.com/admin/users) and remove from [gitlab group](https://staging.gitlab.com/groups/gitlab/group_members).
1. [ ] IT Ops: [Check if the team member has created any Slack bots](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/) before disabling account. [Link](https://gitlab.slack.com/apps/manage?utm_source=in-prod&utm_medium=inprod-apps_link-slack_menu-click) to Apps menu.
1. [ ] IT Ops: Disable team member in [Slack](https://gitlab.slack.com/admin). If the team member is an Admin ping @pkaldis to remove them.
1. [ ] IT Ops: Okta should now deprovision 1Password as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #it-help Slack channel.
1. [ ] IT Ops: Reach out to former team member to identify and retrieve any company supplies/equipment. @gitlab-com/business-technology/team-member-enablement See the [Offboarding page](https://about.gitlab.com/handbook/people-group/offboarding/) for further details on that process.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Comment in this issue when equipment has been returned/recycled. Ping hiring manager, Security and @accounts-payable.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Inform Controller / Accounting if any items in former GitLab Team Member's possession will not be returning, so that they can be removed from asset tracking.
1. [ ] IT Ops: Okta should now deprovision Zoom as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #it-help Slack channel.
1. [ ] IT Ops: Double-check and confirm all Google steps are complete. The Google account will be archived in 90 days for data retention.
1. [ ] IT Ops: Remove access to Loom if person had access


</details>

## Finance

<details>
<summary>Accounting</summary>

1. [ ] Accounting (@accounts-payable): Do not approve any submitted or in progress Expensify expenses until there is confirmation of return of laptop.
1. [ ] Accounting (@accounts-payable): Cancel company American Express card if applicable.
1. [ ] Accounting (@accounts-payable): Remove team member from Expensify.
1. [ ] Accounting (@accounts-payable): Reallocate team members direct reports if any within Expensify for pending and future approvals in alignment with direction provided by the relevant People Business Partner (PBP).
1. [ ] Accounting (@accounts-payable): Check if the team member is an approver in Tipalti or Coupa. Verify there are no pending invoices in their task list.
1. [ ] Accounting (@accounts-payable): Reach out to [TripActions Support](https://app.tripactions.com/app/user/search) to determine the best course of action for any pre-booked flights.
1. [ ] Finance: Check if the team member is a member of Allocadia (marketing business partners).

</details>

# Tech Stack System Deprovisioning / Offboarding - Tasks that need to be completed within 5 days

## System Admins


Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a system you administer access to:**

- Review and remove the former team member's access.
- Select the applicable checkbox(es) once access has been removed from the impacted system.

**If the user DOES NOT have access to a system you administer access to:**

- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.


## Community Relations

#### <summary>Liam McAndrew @lmcandrew</summary>
- Lower the team member's permissions on [Crowdin](https://translate.gitlab.com/project/gitlab/settings#members) to translator

<table>
	<tbody>
		<tr>
			<td> CrowdIn </td>
			<td>

- [ ] Access Deprovisioned - Crowdin
- [ ] Not Applicable to Team Member - Crowdin

</td>
		</tr>
	</tbody>
</table>

## Data

#### <summary>Dennis van Rooijen @dvanrooijen2 Lakshmi Addula @laddula</summary>

<table>
	<tr>
			<td> Snowflake </td>
			<td>

- [ ] Access Deprovisioned - Snowflake
- [ ] Not Applicable to Team Member - Snowflake

</td>
		</tr>
    <tr>
			<td> Stitch </td>
			<td>

- [ ] Access Deprovisioned - Stitch
- [ ] Not Applicable to Team Member - Stitch

</td>
		</tr>
		<tr>
			<td> Fivetran </td>
			<td>

- [ ] Access Deprovisioned - Fivetran
- [ ] Not Applicable to Team Member - Fivetran

</td>
		</tr>
		<tr>
			<td> Airflow </td>
			<td>

- [ ] Access Deprovisioned - Airflow
- [ ] Not Applicable to Team Member - Airflow

</td>
		</tr>
		<tr>
			<td> Sisense </td>
			<td>

- [ ] Access Deprovisioned - Sisense
- [ ] Not Applicable to Team Member - Sisense

</td>
		</tr>
</table>

## Team member Enablement (IT)

#### <summary>Mohammed Al Kobaisy @malkobaisy </summary>

<table>
	<tbody>
		<tr>
			<td> PagerDuty  </td>
			<td>

- [ ] Access Deprovisioned - PagerDuty
- [ ] Not Applicable to Team Member - PagerDuty

</td>
		</tr>
	</tbody>
</table>


#### <summary> @gitlab-com/business-technology/team-member-enablement </summary>

<table>
	<tbody>
		<tr>
			<td> LucidChart  </td>
			<td>

- [ ] Access Deprovisioned - LucidChart
- [ ] Not Applicable to Team Member - LucidChart

</td>
		</tr>
	</tbody>
</table>

## Finance

#### <summary> Finance Ops/Finance System Admins @gitlab-com/business-technology/enterprise-apps/financeops </summary>

<table>
	<tbody>
		<tr>
			<td> Adaptive Insights  </td>
			<td>

- [ ] Access Deprovisioned - Adaptive Insights
- [ ] Not Applicable to Team Member - Adaptive Insights

</td>
		</tr>
		<tr>
			<td> Avalara  </td>
			<td>

- [ ] Access Deprovisioned - Avalara
- [ ] Not Applicable to Team Member - Avalara

</td>
		</tr>
		<tr>
			<td> Coupa  </td>
			<td>

- [ ] Access Deprovisioned - Coupa
- [ ] Not Applicable to Team Member - Coupa

</td>
		</tr>
		<tr>
			<td> Docusign  </td>
			<td>

- [ ] Access Deprovisioned - Docusign
- [ ] Not Applicable to Team Member - Docusign

</td>
		</tr>
		<tr>
			<td> Netsuite  </td>
			<td>

- [ ] Access Deprovisioned - Netsuite
- [ ] Not Applicable to Team Member - Netsuite

</td>
		</tr>
			<td> Stripe  </td>
			<td>

- [ ] Access Deprovisioned - Stripe
- [ ] Not Applicable to Team Member - Stripe

</td>
		</tr>
		<tr>
			<td> Tesorio  </td>
			<td>

- [ ] Access Deprovisioned - Tesorio
- [ ] Not Applicable to Team Member - Tesorio

</td>
		</tr>
		<tr>
			<td> Tipalti  </td>
			<td>

- [ ] Access Deprovisioned - Tipalti
- [ ] Not Applicable to Team Member - Tipalti

</td>
		</tr>
		<tr>
			<td> Zuora Billing </td>
			<td>

- [ ] Access Deprovisioned - Zuora Billing
- [ ] Not Applicable to Team Member - Zuora Billing

</td>
		</tr>
		<tr>
			<td> Zuora Revenue </td>
			<td>

- [ ] Access Deprovisioned - Zuora Revenue
- [ ] Not Applicable to Team Member - Zuora Revenue

</tbody>
</table>




#### <summary>Toni Dominique @tdominique</summary>

<table>
	<tbody>
		<tr>
			<td> Carta  </td>
			<td>

- [ ] Access Deprovisioned - Carta
- [ ] Not Applicable to Team Member - Carta

</td>
		</tr>
	</tbody>
</table>

## Legal

#### <summary>Robert Nalen @rnalen, Kristen Tesh @ktesh </summary>

<table>
	<tbody>
		<tr>
			<td> ContractWorks  </td>
			<td>

- [ ] Access Deprovisioned - ContractWorks
- [ ] Not Applicable to Team Member - ContractWorks

</td>
		</tr>
		<tr>
			<td> Visual Compliance  </td>
			<td>

- [ ] Access Deprovisioned - Visual Compliance
- [ ] Not Applicable to Team Member - Visual Compliance

</td>
		</tr>
		<tr>
			<td> Navex  </td>
			<td>

- [ ] Access Deprovisioned - Navex
- [ ] Not Applicable to Team Member - Navex

</td>
		</tr>
	</tbody>
</table>

## Marketing

#### <summary>Aricka Flowers @atflowers </summary>

<table>
	<tbody>
		<tr>
			<td> YouTube  </td>
			<td>

- [ ] Access Deprovisioned - YouTube
- [ ] Not Applicable to Team Member - YouTube

</td>
		</tr>
	</tbody>
</table>

#### <summary>Michael Preuss @mpreuss22 </summary>

<table>
	<tbody>
		<tr>
			<td> LaunchDarkly  </td>
			<td>

- [ ] Access Deprovisioned - LaunchDarkly
- [ ] Not Applicable to Team Member - LaunchDarkly

</td>
		</tr>
	</tbody>
</table>

#### <summary>Niall Cregan @ncregan </summary>

<table>
	<tbody>
		<tr>
			<td> SEOTesting.com </td>
			<td>

- [ ] Access Deprovisioned - SEOTesting.com
- [ ] Not Applicable to Team Member - SEOTesting.com

</td>
		</tr>
	</tbody>
</table>


#### <summary>Cristal McClure @cmcclure1</summary>

<table>
	<tbody>
		<tr>
			<td> Allocadia  </td>
			<td>

- [ ] Department budget holder: Access Deprovisioned - Allocadia
- [ ] Not Applicable to Team Member - Allocadia

</td>
		</tr>
		</tr>
	</tbody>
</table>

#### <summary>Jameson Burton @jburton </summary>

<table>
	<tbody>
		</tr>
		<tr>
			<td> LinkedIn Sales Navigator  </td>
			<td>

- [ ] Access Deprovisioned - LinkedIn Sales Navigator
- [ ] Not Applicable to Team Member - LinkedIn Sales Navigator

</td>


</td>
		</tr>
		<tr>
			<td> Marketo  </td>
			<td>

- [ ] Access Deprovisioned - Marketo
- [ ] Not Applicable to Team Member - Marketo

</td>
		</tr>

<tr>
			<td> Google Analytics  </td>
			<td>

- [ ] Access Deprovisioned - Google Analytics
- [ ] Not Applicable to Team Member - Google Analytics

</td>
		</tr>
		<tr>
			<td> Google Tag Manager  </td>
			<td>

- [ ] Access Deprovisioned - Google Tag Manager
- [ ] Not Applicable to Team Member - Google Tag Manager

</td>
		</tr>
		<tr>
			<td> Google Adwords  </td>
			<td>

- [ ] Access Deprovisioned - Google Adwords
- [ ] Not Applicable to Team Member - Google Adwords

</td>
	</tbody>
</table>

#### <summary>Wil Spillane @wspillane</summary>

<table>
	<tbody>
		<tr>
			<td> Bambu by Sprout Social </td>
			<td>

- [ ] Access Deprovisioned - Bambu by Sprout Social
- [ ] Not Applicable to Team Member - Bambu by Sprout Social

</td>
		</tr>
	</tbody>
</table>

## People Tools and Technology

#### <summary>Marissa Ferber @MarissaFerber</summary>

<table>
	<tbody>
		</tr>
		<tr>
			<td> LinkedIn Recruiter  </td>
			<td>

- [ ] Access Deprovisioned - LinkedIn Recruiter
- [ ] Not Applicable to Team Member - LinkedIn Recruiter

</td>
		</tr>
		<tr>
			<td> [Greenhouse](https://app2.greenhouse.io/dashboard#)  </td>
			<td>

- [ ] Access Deprovisioned - Greenhouse
- [ ] Not Applicable to Team Member - Greenhouse
- @gitlab-com/gl-ces will complete their off-boarding tasks prior to the deactivation of the team member's Greenhouse account

</td>
	</tbody>
</table>
- If the deactivated team member is a member of the Recruiting Team, please reassign their seat to their Manager.

    - Click Edit > Reassign seat > "... click here to reassign by a member's name" > Reassign

#### <summary>@gitlab-com/gl-ces</summary>

- If Recruiting Ops has already completed their steps:

1. [ ] Search and Update the Hiring Repo, if applicable.
1. [ ] Update the Guide Interviewers.

- If Recruiting Ops has not completed their steps:

1. [ ] Search for any currently scheduled Interviews in the Google Interview Calendar by searching for the team member's name in the Google Calendar Search Bar. If applicable, assign the interviews to another team member in Greenhouse and notify the recruiter.
1. [ ] Check Interview Plans and requisition Hiring Manager fields for the reqs assigned to the team member by searching the team member’s name under Users in Greenhouse and update plans and details accordingly.
1. [ ] Post in the recruiting-team-ces channel the departing team member's name and list reqs interviews were currently scheduled for. Advise the rest of the CES team to review any other interview plans and update accordingly if applicable.
1. [ ] Search and Update the Hiring Repo, if applicable.
1. [ ] Update the Guide Interviewers.
1. [ ] Notify @recruitingops upon completion of tasks.


#### <summary>Learning and Development @jallen16</summary>

<table>
	<tbody>
		<tr>
			<td> Will Learning  </td>
			<td>

- [ ] Access Deprovisioned - Will Learning
- [ ] Not Applicable to Team Member - Will Learning

</td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td> LinkedIn Learning  </td>
			<td>

- [ ] Access Deprovisioned - LinkedIn Learning
- [ ] Not Applicable to Team Member - LinkedIn Learning

</td>
		</tr>
	</tbody>
</table>


## Sales

### <summary>Amber Stahn @Astahn, Jeny Bae @jenybae</summary>

<table>
	<tbody>
		<tr>
			<td> Salesforce  </td>
			<td>

- [ ] Access Deprovisioned - Salesforce
- [ ] Not Applicable to Team Member - Salesforce

</td>
		</tr>
		<tr>
			<td> Salesforce - Reference Edge  </td>
			<td>

- [ ] Access Deprovisioned - Salesforce - Reference Edge
- [ ] Not Applicable to Team Member - Salesforce - Reference Edge

</td>
		</tr>
		<tr>
			<td> Salesforce - Gainsight  </td>
			<td>

- [ ] Access Deprovisioned - Salesforce - Gainsight
- [ ] Not Applicable to Team Member - Salesforce - Gainsight

</td>
		</tr>
		<tr>
			<td> Clari  </td>
			<td>

- [ ] Access Deprovisioned - Clari
- [ ] Not Applicable to Team Member - Clari
- [ ] Remove forecasting (For commercial) - Clari

</td>
		</tr>
		<tr>
			<td> Chorus  </td>
			<td>

- [ ] Access Deprovisioned - Chorus
- [ ] Not Applicable to Team Member - Chorus

</td>
		</tr>
		<tr>
			<td> LinkedIn Sales Insights  </td>
			<td>

- [ ] Access Deprovisioned - LinkedIn Sales Insights
- [ ] Not Applicable to Team Member - LinkedIn Sales Insights

</td>
		</tr>
	</tbody>
</table>

### <summary>Caitlin Ankney @cankney Emily McInerney @emcinerney</summary>

<table>
	<tbody>
		<tr>
			<td> Gainsight  </td>
			<td>

- [ ] Access Deprovisioned - Gainsight
- [ ] Not Applicable to Team Member - Gainsight

</td>
		</tr>
	</tbody>
</table>

## Security

#### <summary>James Ritchey @jritchey, Ethan Strike @estrike </summary>

<table>
	<tbody>
		<tr>
			<td> HackerOne  </td>
			<td>

- [ ] Access Deprovisioned - HackerOne
- [ ] Not Applicable to Team Member - HackerOne

</td>
		</tr>
	</tbody>
</table>

## Support

#### <summary>Nabeel Bilgrami @nabeel.bilgrami </summary>

<table>
	<tbody>
		<tr>
			<td> Zendesk Light Agent  </td>
			<td>

- [ ] Access Deprovisioned - Zendesk Light Agent
- [ ] Not Applicable to Team Member - Zendesk Light Agent

</td>
		</tr>
		<tr>
			<td> Zendesk - Remove the agents 'user tags'  </td>
			<td>

- [ ] Access Deprovisioned - Zendesk - Remove the agents 'user tags'
- [ ] Not Applicable to Team Member - Zendesk - Remove the agents 'user tags'

</td>
		</tr>
	</tbody>
</table>

## UX

#### <summary>Adam Smolinski @asmolinski2</summary>

<table>
	<tbody>
		<tr>
			<td> Qualtrics  </td>
			<td>

- [ ] Access Deprovisioned - Qualtrics
- [ ] Not Applicable to Team Member - Qualtrics

</td>
		</tr>
	</tbody>
</table>

## Professional Services

### <summary>Kendra Marquart @kmarquart</summary>

<table>
	<tbody>
		<tr>
			<td> Badgr  </td>
			<td>

- [ ] Access Deprovisioned - Badgr
- [ ] Not Applicable to Team Member - Badgr

</td>
		</tr>
	</tbody>
</table>


---

# Job Specific Offboarding Task
If a task only applies to a Entity, Country, Division, Department or Role it will appear under here.


<!-- include: entity_tasks -->

---

<!-- include: country_tasks -->

---

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: people_manager_tasks -->
