## FOR SECURITY ONLY

- [ ] @jritchey @estrike: Remove the team member from HackerOne
- [ ] @jritchey @estrike: Remove the team member from hackerone-customer.slack.com slack workspace
- [ ] @mlancini: Remove the team member from Tenable.IO
- [ ] @mlancini: Remove the team member from Rackspace (Security Enclave)
- [ ] @mlancini: Remove the team member from AWS Security
- [ ] @blutz1: Remove the team member from Panther SIEM
- [ ] @rcshah @mmaneval20: Remove the team member from ZenGRC
    - [ ]@rcshah export all open assigned objects and create a task for Manager to review for re-assignment
- [ ] @darawarde @sdaily: Remove the team member from Google Search Console
- [ ] @jburrows001 @rcshah: Remove team member from PhishLabs
- [ ] @julia.lake @mmaneval20: Remove the team member from BitSight



