### FOR UX RESEARCHERS & PRODUCT DESIGNERS

<summary>Manager</summary>

1. [ ] Manager: Remove former team member's `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: Remove former team member from [Mural](https://www.mural.co/)
1. [ ] @vkarnes: Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud)
1. [ ] @asmolinski2: Remove former team member from [Dovetail]
1. [ ] @asmolinski2: Remove former team member from [Qualtrics](https://qualtrics.com)
1. [ ] @asmolinski2: Remove former team member from UserTesting.com
1. [ ] @cfaughnan: Remove former team member from Respondent
1. [ ] @asmolinski2: Remove former team member from UX team Google calendar

<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : remove team member from the `@uxers` User Group on Slack.

<summary>Other</summary>

1. [ ] @tauriedavis or @jeldergl: Remove former team member from [Figma]
