## FOR MARKETING ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions

      * [ ]   Salesforce - @jenybae @Astahn: REASSIGN RECORDS in Salesforce: Reassign all accounts and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!).
      * [ ]   Salesforce - @bethpeterson : REASSIGN RECORDS in Salesforce: Reassign all leads and contacts.
      * [ ]   Salesforce - @sheelaviswanathan: REMOVE FROM PICKLISTS: remove from SDR or BDR picklists on ACCOUNT and OPPORTUNITY objects and if applicable, replace the former SDR/BDR to new SDR/BDR on both the account and opportunity objects. Please do not replace the SDR/BDR CLOSED WON/LOST OPPS and only replace opportunities with a new SDR/BDR if the opportunity is not yet qualified

      * [ ]   Outreach - @gillmurphy: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   ZoomInfo - @RobRosu: Deactivate and remove former team member from ZoomInfo.
      * [ ]   Marketo - @jburton: Remove from Workflow Campaigns, pick lists, and SFDC assignment sync.
      * [ ]   ReachDesk - @jburton: Remove team member from ReachDesk if applicable.
      * [ ]   LinkedIn Sales Navigator - @jburton: Disable user, remove from team license.
      * [ ]   Drift - @bethpeterson: IF User: Disable access; IF Admin: Reset password.
      * [ ]   LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
      * [ ]   1Password - @davidbrown1 : Rotate any shared login credentials (SFDC API user, Google Analytics, Adwords, Disqus, Facebook, LinkedIn, Zendesk).
      * [ ]   Ringlead - @jenybae, @Astahn: Remove team member from Ringlead if applicable.
      * [ ]   Demandbase - @bethpeterson: Remove team member from Demandbase if applicable.
      * [ ]   ARInsights' Architect - Ryan Ragozzine @rragozzine: Remove team member from ARInsights' Architect if applicable.
      * [ ]   Terminus @gillmurphy: Remove team member from Sigstr if applicable
      * [ ]   PathFactory @MihaiConteanu: Remove team member from PathFactory if applicable
      * [ ]   LogRocket @jburton: Remove team member from LogRocket if applicable
      * [ ]   Litmus @amy.waller: Remove team member from Litmus if applicable
      * [ ]   MailJet @amy.waller: Remove team member from MailJet if applicable
      * [ ]   MailChimp @kjaeger: Remove team member from MailChimp if applicable
      * [ ]   Rev @gillmurphy: Remove team member from Rev if applicable
      * [ ]   Facebook Ad Platform @mnguyen4: Remove team member from Facebook Ad Platform.
      * [ ]   Google Search Console @jburton: Remove the team member from Google Search Console
      * [ ]   Shopify @suripatel: Remove the team member from Shopify
      * [ ]   DV 360 @mnguyen4: Remove team member from DV 360 if applicable
      * [ ]   Frame.io @atflowers: Remove team member from Frame.io if applicable
      * [ ]   Figma @jburton: Remove team member from Figma if applicable
      * [ ]   Keyhole @gillmurphy: Remove team member from Keyhole if applicable
      * [ ]   iconik @MihaiConteanu: Remove team member from iconik if applicable
      * [ ]   Vimeo @MihaiConteanu: Remove team member from Vimeo if applicable
      * [ ]   OneTrust @jburton: Remove team member from OneTrust if applicable
      * [ ]   Sitebulb @jburton: Remove team member from Sitebulb if applicable
      * [ ]   Swiftype @jburton: Remove team member from Swiftype if applicable
      * [ ]   LaunchDarkly @jburton: Remove team member from LaunchDarkly if applicable
      * [ ]   SEMrush @jburton: Remove team member from SEMrush if applicable
      * [ ]   Smartling @gillmurphy: Remove team member from Smartling if applicable

1. [ ] IMPartner - @ecepulis @KJaeger: Remove team member from IMPartner if applicable.
1. [ ] Canva - @luke: Remove team member from Canva if applicable
1. [ ] Xactly Incent - @lisapuzar @Swethakashyap @pravi1 @sophiehamann


