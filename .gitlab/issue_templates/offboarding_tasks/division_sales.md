## FOR SALES ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
    * [ ] @Astahn @jenybae: Update Sale Operations Sponsored Reports/Dashbaords listed on [The Sales Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance)
    * [ ] @Astahn @jenybae: To offboard Accounts and Opportunities, create a an issue in the [Sales Operations project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/new?issue%5Bmilestone_id%5D=#) and use the Territory Change Request template.  Select offboarding as the reason and assign to the sales manager and the related Sales Opeartions team member (if unknown ask in the sales operations slack channel).
    * [ ] @Astahn @jenybae: If offboarding Sales ASM/RD/VP, replace the manager field in SFDC with the updated manager
    * [ ] @bethpeterson Salesforce Leads and contacts: REASSIGN all leads and contacts based on direction provided by SDR leadership within 24 hours of termination.
    * [ ] @bethpeterson: Update Territory Model in LeanData with temporary territory assignments for SDRs.
    * [ ] Outreach - @gillmurphy: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
    * [ ] ZoomInfo - @robrosu: Deactivate and remove former team member from ZoomInfo
    * [ ] LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
    * [ ] LinkedIn Sales Navigator - @jburton Disable user, remove from team license.
1. [ ] Ringlead: @Astahn @jenybae Remove team member from Ringlead if applicable.
1. [ ] Terminus: @gillmurphy Remove team member.
1. [ ] IMPartner: @ecepulis @KJaeger Remove team member from IMPartner if applicable.
1. [ ] Xactly Incent: @lisapuzar @Swethakashyap @pravi1 @sophiehamann
1. [ ] O'Reilly: @jfullam @tbopape Remove team member from O'Reilly if applicable.
