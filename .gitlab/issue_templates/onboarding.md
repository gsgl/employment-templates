## Welcome to your GitLab Onboarding Issue! :tada:

We are so excited you’re here and look forward to setting you up for [all-remote](https://about.gitlab.com/company/culture/all-remote/guide/) success as your team member journey begins.

**⚠ Note: The wealth of information provided in this issue can be overwhelming at times. It's okay to skim through resources, bookmark them and come back at a later time.**

![swag](https://gitlab.com/gitlab-com/people-group/peopleops-eng/emails/-/raw/f300f5097b8364158541bc447ad71af2d6fad249/images/swag.png)

### Table of Contents

* [Accounts and Access](#accounts-and-access)
* Day 1: [Getting Started: Accounts and Paperwork](#day-1-getting-started-accounts-and-paperwork)
* Day 2: [Morning: Remote Working and our Values](#day-2-remote-working-and-our-values)
* Day 3: [Morning: Security & Compliance](#day-3-security-compliance)
* Day 4: [Morning: Social & Benefits](#day-4-social-benefits)
* Day 5: [Morning: Git](#day-5-git)
* [Weeks 2 - 4: Explore](#weeks-2-4-explore)
* [Job Specific Tasks](#job-specific-tasks)

### Introduction

This is your core GitLab onboarding issue and it is housed in GitLab.com in the interests of what is known as [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding). It has been crafted with the [three dimensions of remote onboarding](https://about.gitlab.com/company/culture/all-remote/onboarding/) in mind, namely:
- Organizational Onboarding
- Technical Onboarding
- Social Onboarding

All three of these are underpinned by the sentiments of [self-directed and continuous learning](https://about.gitlab.com/company/culture/all-remote/self-service/), using the diverse set of high and low touch resources available to all GitLab team members such as videos; handbook pages; blog pieces; Zoom sessions and knowledge assessments, while encouraging [asynchronous communication and workflows](https://about.gitlab.com/company/culture/all-remote/asynchronous/).

### Support and our Single Source of Truth

Since GitLab is a [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/) organization, the answer to any question you may have particularly throughout your first thirty days should be documented and readily available as this is our [Single Source of Truth (SSoT)](https://about.gitlab.com/handbook/values/#single-source-of-truth). 

Clicking on any of the links detailed within the body of this issue will move you away from this page i.e. it is recommended that you rather right-click and open links in a new tab if you would like to keep this page handy.  

Additionally, it should be noted that team members are not expected to read or memorize the GitLab Team Member Handbook in its entirety - this issue documents the elements pertinent to your onboarding and we encourage you to bookmark those pages that you would like to refer back to for a more detailed review to once you have had time to settle.

If you are unable to find the information you are looking for, please be sure to reach out to your Manager (`__MANAGER_HANDLE__`); Onboarding Buddy (`__BUDDY_HANDLE__`) or People Experience Associate (PEA) (`__PEOPLE_EXPERIENCE_HANDLE__`) for support by tagging them in the comments section of this issue. If you need to troubleshoot specific issues with tools, you can add a screenshot of the issue directly in the comment to help them diagnose the problem.

Alternatively once you are active on [Slack](https://about.gitlab.com/handbook/communication/#key-slack-channels), which is used primarily for informal communication, you will notice that you have been automatically added to a handful of useful support channels such #questions and #it-help.

### Helpful Tools

1. [TaNEWki Tips](https://about.gitlab.com/handbook/people-group/general-onboarding/tanewki-tips/) - This page is here to help walk you through what you can expect before and during your first few weeks as a new team member.
1. [Select and Speak - Text to Speech](https://chrome.google.com/webstore/detail/read-aloud-a-text-to-spee/hdhinadidafjejdhmfkjgnolgimiaplp?hl=en)
1. [Read Aloud: A Text to Speech Voice Reader](https://chrome.google.com/webstore/detail/select-and-speak-text-to/gfjopfpjmkcfgjpogepmdjmcnihfpokn?hl=en)
1. [Rectangle App](https://rectangleapp.com/) - Move and resize windows with ease
1. [OneTab](https://www.one-tab.com/) - Tab organizer for Chrome, Firefox, Edge, or Safari

### The First Thirty Days

This issue consists of a series of tasks which should be completed sequentially over the course of the next thirty days structured as follows:

| Day | Area of Focus |
| ----- | ----- |
| **Day 01** | Accounts; Applications and Paperwork |
| **Day 02** | Remote Work; Communication and GitLab Values |
| **Day 03** | Security |
| **Day 04** | Organizational Structure and Social Elements |
| **Day 05** | Introduction to Git and using GitLab |
| **Day 06+** | Explore (Various) |

Though you may be eager to jump right into your role we encourage you to set aside dedicated time of at least one to two weeks, to focus on these tasks as they are geared toward ensuring you are enabled to [thrive in an all-remote environment](https://about.gitlab.com/company/culture/all-remote/onboarding/#the-importance-of-onboarding).

You will however see that afternoons have been allocated to role-based onboarding which can be found at the bottom of the issue above the comments section.

### :red_circle:  Onboarding Compliance

It is important to note that certain tasks within your onboarding issue relate to compliance and ultimately keeping GitLab secure.

These tasks are marked with a red circle :red_circle: and completion of them is subject to audit by both the People Experience Team and various other stakeholders such as Security and Payroll so please be sure to acknowledge them once complete by checking the relevant box.

***Please note that you must complete these tasks***

### GitLab Learn

We host most of our onboarding trainings in EdCast, or as we call it, [GitLab Learn](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/). You will learn more about this system on Day 2 of Onboarding. You can also access your training through the links listed throughout the issue.

To access GitLab Learn, you will receive an email notification in your GitLab Gmail account.

After clicking on the invitation link, follow these steps to access the trainings:

Go to` My Profile` -> `My Learning Plan` to see your assigned content.


### Continuous Improvement

In the interests of paying it forward and contributing to our core values of [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) - we encourage you to suggest changes and updates to this issue and the handbook, both of which can be done via Merge Request (MR), which you will learn about on Day Five. The easiest way to search for anything within this issue template is to hit the `Edit` button - the small pencil icon in the top right of the issue - followed by `Command + F` which will open the search bar into which you can enter the word or phrase you are looking for.

Along with this, you will close out your issue on Day Thirty by being asked to complete your [Onboarding Satisfaction Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform), which is an opportunity to share feedback on both your general Onboarding Experience and your Onboarding Buddy Experience.

This is another opportunity for the People Experience Team to evolve and improve upon the program using the feedback provided and also allows for your Onboarding Buddy to be included in the [Quarterly Raffle](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#buddy-program).

---

### Before Starting at GitLab

<details>
<summary>Manager Tasks</summary>

1. [ ] Select an onboarding buddy on the new team member's team. Please select someone in a similar time zone and someone that will not be on PTO the first 2 weeks. Ideally, someone who has been at GitLab for at least 3 months would be helpful. It is highly encouraged that an onboarding buddy is selected for the new team member to have the best support during their onboarding. Review [Onboarding Buddies Handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) for more information.
1. [ ] Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Assign the onboarding buddy to this issue by clicking Edit in the Assignees section (right-hand column) and enter the buddy GitLab handle. Please also update the onboarding `__BUDDY_HANDLE__` in the top intro section of this issue with the buddy handle by clicking edit in this issue. This will allow the onboarding buddy to have even further transparency and awareness on how the new team member is doing.
1. [ ] We encourage managers to review their schedule and ensure availability to support onboarding team members ahead of their pre-determined start date. In an instance where you may be OOO please be sure to nominate another individual from within the department for this purpose.
1. [ ] The default laptop for the new team member will be an Apple MacBook Pro. If the new team member could benefit from using a Dell with Linux instead, remind the new team member to request it. Details and the appropriate information on laptops are included in the "Welcome" email sent to the new team member from the CES team after the team member's offer is signed, but many are overwhelmed with paperwork and new job excitement, so a friendly reminder might be in order. Refer to the handbook for more information on [approved laptops](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptop-configurations).
1. [ ] Schedule a Zoom video call with the new team member for the start of their first day to welcome them to the team and set expectations. Send them an invitation to their personal email. The team member's GitLab email address will only be activated on their first day.
1. [ ] Send an email to the new team member's personal email address welcoming them to GitLab. Provide them with the time and Zoom link to the introduction video call as well as any other information the manager feels will help with a great first day experience.
1. [ ] If the new team member has a LinkedIn profile, share this link with the rest of the team via Slack, encouraging them to reach out to the new team member to start building collaboration and excitement.
1. [ ] Read through the [Building Trust Handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/building-trust/). All people leaders have a responsibility to get to know their people to understand how someone would like to receive feedback.
1. [ ] Organize a smooth onboarding plan with clear starting tasks and pathway for new team member.
1. [ ] Review [GitLab's Probation Period Process](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/) and check if your new team member falls within a country that has a probation period. This is important as the People Experience team will be following up with you to review the probation period, within the period on the linked page. <!-- Intern: Remove -->
1. [ ] Review any Job-specific tasks add at the bottom of this issue. Complete any manager tasks and coordinate with the People Experience Team if any tasks need to be updated.
1. [ ] If this team member will be a People Manager, please note that the Interview Training and Becoming a Manager issues will be [opened](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#create-interview-training-issue) automatically, one week after their start date to make it less overwhelming their first week. <!-- Intern: Remove -->
1. [ ] Add [Manager: Pre-Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks. [Labels](https://docs.gitlab.com/ee/user/project/labels.html) are used to track which tasks may still be incomplete for the People Experience team.

</details>

<!-- Intern: Update to Mentor -->
<details>
<summary>Buddy Tasks</summary>

1. [ ] Review your schedule, if you will be taking extended PTO (more than a few days) or going out on leave, please alert the team members manager and ask for a new onboarding buddy to be assigned.
1. [ ] Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Ask Manager for the new team member's personal email by email (not in this issue). Managers are cc'd when the Candidate Experience Team sends the new team member a Welcome email to their personal email address once the agreement is signed.
1. [ ] Once you have the personal email address, feel free to reach out to the new team member to introduce yourself over email using this [email template](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#example-email-template).
1. [ ] Set a reminder to schedule a Zoom call on the team member's first day to introduce yourself "in person". Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc. **Please note that if you send an invite to their GitLab Gmail account, it will bounce before their first day, so it's best to send an invite to their personal email if scheduling the coffee chat in advance**
1. [ ] Consider creating an agenda/training doc for the team member to ask questions async.
1. [ ] Schedule at least one follow-up chat on their second week and third week. During that chat, offer to help them with updating the team page when they hit that task on the onboarding. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.
1. [ ] Check in with the new team member and see if they need help scheduling any coffee chats.

:tada: Don't forget that there is a quarterly onboarding buddy raffle where all buddies stand a chance to win a discount code to the swag store. Give it your best!

</details>

<details>
<summary>People Experience Tasks</summary>

**Issue Review**
   1. [ ] Assign this issue to the People Experience team member and the hiring manager (this should be done automatically, but worth checking) and check that the following information is accurate:

| Information | Value |
| :---------- | :---- |
| BAMBOOHR_ID | `__BAMBOO_ID__` |
| MANAGER | `__MANAGER_HANDLE__` |
| PEOPLEOPS EXPERIENCE SPECIALIST | `__PEOPLE_EXPERIENCE_HANDLE__` |

**Bamboo HR**
   1. [ ] Check if new team member was referred and by whom in [Greenhouse](https://app2.greenhouse.io/dashboard). If it's not marked at the top of their Greenhouse profile, go to "Application" on the lefthand sidebar under the job they were hired for, then click "View Job Post" and look in their application questions to see if they mentioned a referrer. Make sure the source for the candidate is labeled as referred and by the appropriate person. There is also an alert in Slack that check's for referrals.
   1. [ ] If the new team member was referred, add a note under "Notes" in the team member's BambooHR profile with the name of who referred them. Also [add the referral bonus](https://about.gitlab.com/handbook/incentives/#referral-bonuses) for the referrer in BambooHR. Please verify based on the handbook what the amount should be depending on if the hire is in a low location area. In terms of determining if they are part of an [underrepresented group](https://about.gitlab.com/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups), please follow the relevant task on Day 1.
   1. [ ] If the new team member listed any exceptions to the IP agreement in their contract, choose `Yes` in the Exception to the IP agreement field in the Jobs tab.
   1. [ ] Check to make sure the department of the team member matches the manager's department.

**Communication**
1. [ ] Check to see whether the manager is or is planning any PTO during the first week of onboarding. Contact an alternative team member to assist if this is the case.
1. [ ] Add the following comment to the issue and tag the manager.
> I have created the onboarding issue; inside this issue, there are tasks for you and the onboarding buddy (you will assign). A portion of those tasks must be completed before the start date to ensure a successful onboarding process. We also suggest allowing for the new team member to have two full weeks to focus only on this onboarding issue. Please let me know if you have any questions.
1. [ ] Notify the manager of the new team member's GitLab email address, don't forget to mention that the email account will only be activated on the team member's start date (this will avoid any confusion with returning mail delivery failures).
1. [ ] Review the Onboarding Buddy tasks have been completed, if not send a friendly ping in this issue.
1. [ ] Review the Manager Pre-Start tasks have been completed, if not send a friendly ping in slack.
1. [ ] Add [PE: Pre-Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks.


</details>

---

### Accounts and Access

<details>
<summary>People Experience Tasks</summary>

#### Day 1

**Calendars & Agenda**
1. [ ] Add new team member to the next quarterly [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/) in the GitLab Team Meetings calendar for EMEA/US team member. Also, invite new team member to the "CEO 101 Introductions" meeting that takes place right before it. When prompted to "Edit recurring event", choose "This event".
      - If the new team member is in APAC, please add to the APAC CEO AMA. When prompted to "Edit recurring event", choose "This event".
1. [ ] Add new team member to the next onboarding check in call for their appropriate timezone. 
1. [ ] Add new user as a Manager to the [GitLab Unfiltered YouTube account](https://myaccount.google.com/u/3/brandaccounts/103404719100215843993/view?pli=1) by clicking "Manage Permissions" and entering their GitLab email address.
1. [ ] If the team member was referred, review their self-identification data in BambooHR, specifically the Gender, Ethnicity, and Veteran status. If any of these place the team member in an [underrepresented group](https://about.gitlab.com/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups) please update their referrer's Referral bonus amount.
1. [ ] Request signature of the following documents in BambooHR:
- Code of Conduct 2021
- Acknowledgement of Relocation 2021
- Consent to Collect and Use Data Regarding Race Ethnicity **Please do NOT send this to team members located in: France, Belgium, Denmark, Finland, Iceland, Norway, or Sweden for compliance reasons**
- Social Media Policy Acknowledgment
1. [ ] Log into [1Password](https://gitlab.1password.com/people) and resend an invite to the new team member's GitLab email address.
1. [ ] Add [PE: Day 1- Complete Label](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Compliance
**After Day 1**
1. [ ] Verify that the new team member has checked the New Hire Security Orientation video training task (Day 1 Morning - Security Tasks).
1. [ ] Verify that the new team member has checked the Survey Security form-related task and submitted a response to the form (Day 1 Morning - Security Tasks).
1. [ ] Verify that the new team member has acknowledged and signed the `Code of Conduct 2021`, `Acknowledgement of Relocation 2021`, `Consent to Collect and Use Data Regarding Race Ethnicity`, and `Social Media Policy Acknowledgment` in BambooHR. You would have received an email to notify you that this has been signed.

**After Day 2**
1. [ ] Move the scanned photo of ID to the Verification Folder in BambooHR.
1. [ ] Rename the identity document to the format `First_Name Last_Name Document_Type`
1. [ ] Important: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR. Update BambooHR if applicable. This is important for audit purposes and to remain compliant. Please ping the team member if the ID / passport has still not been uploaded by the end of Day 2.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`
1. [ ] Invite the team member to the onboarding cohorts Slack channel (new hire lounge) for the specific month.

**After Day 3**
1. [ ] Check to ensure encryption screenshot has been uploaded and an encryption label (`encryption_missing` or `encryption_complete`) has been added to the issue. If it hasn't been added, please ping the team member in the onboarding issue to please complete the task.

**After One Week**
1. [ ] Add a comment to this issue, checking in on the new team member. Check on their progress within the issue.
1. [ ] If the new team member is a Manager, ensure that the `Becoming a Manager` and `Interview Training` issues have been opened and automatically assigned to the team member.

**After 4 Weeks**
1. [ ] Check that the team member has reviewed the handbook by completing Task Number 2 "Read the team handbook".
1. [ ] If the team member has completed the majority or all tasks in the issue, make a comment to congratulate the team member for completing their onboarding and that you will be closing the issue.

</details>

<details>
<summary>BambooHR Tasks</summary>

#### Day 1
1. [ ] People Experience Associate (Ones with BambooHR Provisioner Access):
    * In BambooHR, update access levels for Managers and Contractors, if applicable, as follows:
        * For Employees who are Managers of people: "Managers"
        * For Contractors (independent or corp-to-corp): "Contractor Self-Service"
        * For Contractors who are Managers of people: "Multiple Access Levels": "Contractor Self-Service" and "Managers"
    * In BambooHR, audit the entry and mark complete in the "Payroll Change Report".

</details>

<details>
<summary>Manager Tasks</summary>

#### Day 1

1. [ ] Invite team member to recurring team meetings, consider pressing `don't send`, this will reduce noise in their inbox on day 1.
1. [ ] Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Set new GitLab team member's GitLab group and project-level permissions as needed.
1. [ ] Ensure new GitLab team member knows which individuals they will be collaborating with and working with on a day-to-day basis in their functional groups.
1. [ ] Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.
1. [ ] Onboarding in an all remote environment can be tricky for new teams to find connections. Encourage the team member to join the monthly [`Let's Connect - GitLab Social Call`](https://about.gitlab.com/handbook/communication/#social-call) to stay connected with the wider team.
1. [ ] Add [Day 1- Complete Label](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Day 2

1. [ ] A [role based entitlement Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#role-entitlements-for-a-specific-job) will be created automatically for new team members **if** a [pre-approved template exists](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) and you will be tagged in it automatically (starting 2020-Apr). The AR will also be automatically added to the new team member epic together with their onboarding issue. The AR should request access to anything NOT provided by [baseline entitlements for all GitLab team members](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#baseline-entitlements-all-gitlab-team-members).
    - As the manager, please check if the AR covers the correct entitlements.
    - If the AR was not created AND a role baseline entitlement exists for the role, please create an [issue for People Ops Engineering](https://gitlab.com/gitlab-com/people-group/people-operations-engineering/-/issues/new)  and proceed with manually creating the AR for the new team member. If creating it manually, don't forget to include the [person's details](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request.md) in the issue.
    - For all other roles that do not have a role-specific AR template, open a [Individual or Bulk Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request). We recommend [creating a role based entitlements template](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#how-do-i-create-a-role-based-entitlement-template) so all future team members with a role get the AR created automatically.


#### Week 2

1. [ ] If this team member will be a assigned to one or more [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages), and have successfully added themselves to the team page, add a new entry for the team member in the [_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/includes/product/_categories-names.erb) file.
1. [ ] Open any role based onboarding issue for the team member to start working through, whilst also still completing this issue.

</details>

<details>
<summary>Accounts Payable Tasks</summary>

#### Day 1

1. [ ] Accounts Payable (@accounts-payable): If an applicable policy exists for the employee type and entity, [invite team member](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#add-expensify) to an account in [Expensify](https://www.expensify.com/signin). If contractor, search to see if there is an `iiPay` policy in the currency of the team member's compensation, and invite to that policy if it exists. If none currently exists, create a new policy and the new team member to that policy.

</details>

<details>
<summary>Legal Tasks</summary>

#### Day 1

1. [ ] Legal (@ktesh): Assign training in Navex LMS. 

</details>

---

<!-- include: country_tasks -->

---

<!-- include: entity_tasks -->

---

### Day 1 - Getting Started: Accounts and Paperwork

**Hello! Welcome to Day 1 of onboarding. We could not be more excited you are here! Your first day includes tasks for completing paperwork, security tasks, and getting your core systems in place. This will set you up for the rest of onboarding and beyond.**

<details>
<summary>New Team Member</summary>

1. [ ] Assign the onboarding issue to yourself. Click `Edit` in the Assignees section (right-hand column) and enter your GitLab handle. This will add you to the team members currently assigned. 
1. [ ] If you are already in possession of a company-owned Apple laptop, please create or update the Apple ID, using your GitLab email address. If you have [problems creating an apple ID from system preferences](https://discussions.apple.com/thread/8438000), try creating it through the [App Store](https://support.apple.com/en-us/HT204316). If an app is critical to your work, it can be reimbursed through Expensify. If your laptop is of a different make, please assign any computer or store ID to your GitLab email address.
1. [ ] Take a look at our Onboarding Feedback [page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/) and see what other team members have shared about their onboarding experience.

### :red_circle: Security Tasks
1. [ ] :red_circle: Watch the recorded [New Hire Security Orientation](https://www.youtube.com/watch?v=kNc9f-LH6pU) training.
1. [ ] :red_circle: Familiarize yourself with the process to [engage security on-call](https://about.gitlab.com/handbook/engineering/security/#engaging-the-security-on-call)
1. [ ] :red_circle: Complete the [survey/feedback](https://docs.google.com/forms/d/1_jUbA961Mj44paTfiI2VFTMsElAdjEqFsLrfPKfoW04) form for the Security Orientation training.
1. [ ] :red_circle: Read our [Security Practices](https://about.gitlab.com/handbook/security) page. Ensure to read our [Password Policy Guidelines](https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines).
1. [ ] :red_circle: Jamf enrollment - *Mac only*
    - If you have self-procured your machine, please make sure to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf).
    - If the company has procured the machine for you, please confirm that [Jamf is installed correctly](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#installation-completion-confirmation). If not, please follow the steps to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf). Please ask any questions in the `#it_help` Slack channel.

### :red_circle: 1Password

1. [ ] :red_circle: Install 1Password. GitLab provides all team members with an account to 1Password. Some teams use 1Password vaults for shared notes and some shared accounts. Should your team have a vault, this will be added to your access request issue. 1Password is available as a standalone application for macOS. It is also available as a browser extension and CLI tool for Linux. 
   1. [ ] The 1Password invitation email will be in your GitLab email inbox,. Please search for it and accept the invitation.
   1. [ ] Install the 1Password app or browser extension on your computer. Then link it to your team (GitLab company) account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Tip: Please use Google Chrome on your laptop as our applications are set up in Google environments. Using Safari (Apple users) can cause some issues with setup. Please let People Experience or your manager know if you need any assistance.
1. [ ] Read 1Password's [getting started guides](https://support.1password.com/explore/extension/).
1. [ ] Read [Mac app's guide](https://support.1password.com/getting-started-mac/) to understand its functionality.
1. [ ] Before saving passwords in 1Password, understand why some will be in 1Password versus Okta. Passwords should be saved in your [*Private* 1Password vault](https://support.1password.com/1password-com-items/) if they cannot be saved in Okta. This will make the passwords only accessible to you. If you save passwords in a shared vault they will be accessible to others at GitLab. 

### Slack
1. [ ] Using your @gitlab.com email, register on Slack by following this [invitation link](https://join.slack.com/t/gitlab/signup). Read the next suggestions on how to choose a username first.
1. [ ] Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
1. [ ] Fill in your [Slack profile](https://gitlab.slack.com/account/profile), as we use Slack profiles as our Team Directory to stay in touch with other team members. The fields include:
    1. [ ] Photo
    1. [ ] What I Do (can be your job title or something informative about your role)
    1. [ ] Phone Number including country code
    1. [ ] Time Zone (useful for other GitLab team members to see when you're available)
    1. [ ] GitLab.com profile, set the Display text to your `@gitlabusername`
    1. [ ] Job Description link from our handbook
    1. [ ] City and country
    1. [ ] Working hours, can help others to identify the times you are generally available
    1. [ ] GitLab Birthdays: Opt in or out of the birthday celebrations by selecting `yes` or `no`
    1. [ ] If you'd like to, you can add your personal email address instead of your GitLab email address (this is optional)
    1. [ ] Consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
    1. [ ] Consider adding your pronouns in the `Pronouns` field. By making it normal to set pronouns we create a more inclusive environment for people who use non-traditional pronouns.
    1. [ ] Consider adding pronunciation guides for your full name in the `Pronunciation Guide` to help others to pronounce your name correctly (e.g. sid see-brandy for Sid Sijbrandij).
1. [ ] You have been automatically added to certain Slack channels. Check out some of GitLab's [key Slack channels](https://about.gitlab.com/handbook/communication/#key-slack-channels) to get started. You can also reference the Slack help center for [instructions on browsing all available channels](https://slack.com/help/articles/205239967-Join-a-channel); we have thousands!
1. [ ] Read our handbook section on [communication via Slack](https://about.gitlab.com/handbook/communication/#slack), paying particular attention to the item concerning `Please avoid using @here or @channel unless this is about something urgent and important`.
1. [ ] Optiona: Sign up to be part of an upcoming session of our [Slack training](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/slack-training/)

1. [ ] Introduce yourself in the Slack [#new_team_members](https://gitlab.slack.com/messages/new_team_members/) channel, where you can ask any questions you have and get to know other new team members! Tips:
   1. Make sure your Slack profile has a **photo** - it makes it easier for other GitLab team members to remember you!
   1. We love to hear more, such as where you were before, family, pets, and hobbies.
   1. We also enjoy pictures if you're willing to share. Consider giving your new team members a glimpse into your world. You can scroll through previous messages in the channel for inspiration.
   1. The new_team_members channel is an intro to a wider range of GitLab team members. There are over 1000 team members in the channel.  
1. [ ] You will be automatically added to the [new hire lounge Slack channel]((https://about.gitlab.com/handbook/people-group/people-experience-team/#onboarding-cohort-creation)) on day 2. This channel will include all team members hired for the specific month that you have been hired in. There will be prompts every Tuesday and Thursday. These prompts can be used to connect and get to know other team members, as well as for any onboarding questions/help. It is a great way to connect with other team members, at a smaller level, creating an onboarding cohort. This channel is a great resource for you to connect with other new hires going through similar experiences. You can also ask questions as well as see general announcements that may impact you. 
1. [ ] Set a reminder to introduce yourself in your onboarding co-hort lounge to your fellow new hires when you get access on Day 2.

### :red_circle: Okta

1. [ ] :red_circle: Set up your Okta Account. GitLab uses Okta as its primary portal for all SaaS Applications. You should already have an activation email in both your Gmail and Personal Accounts. Okta will populate your Dashboard with many of the Applications you will need, but some of these will require activation. Read and Review the [Okta Handbook](https://about.gitlab.com/handbook/business-technology/okta/) page for more information.
   1. [ ] Please use Google Chrome on your laptop as our applications are set up in Google environments. Using Safari (Apple users) can cause some issues with setup.
   1. [ ] Please login to your Gitlab account via Okta and [reset your password](https://docs.gitlab.com/ee/user/profile/#change-your-password) in Gitlab. Make sure that you start from the [Okta dashboard](https://gitlab.okta.com/app/UserHome) to log into your GitLab account. Do this by clicking the GitLab.com tile directly from your Okta dashboard. **Note: the GitLab tile can take 24-48 hours to appear in Okta. If this does not appear after this time period, please reach out to #it_help on Slack**
   1. [ ] Set up 2FA for your Google account immediately. Navigate to your [Google Account Settings](https://myaccount.google.com/). Click on the Security tab. Scroll down to the Signing in to Google section of the page and click on 2-step verification. Add a second step for auth.
   1. [ ] If you created any accounts while onboarding before being added to Okta, add these passwords to the relevant Okta Application.
   1. [ ] Reset any additional existing passwords in line with the security training & guidelines.
   1. [ ] Confirm in the comment box that you are using 1Password and Okta in accordance with our Security Practices.

### :red_circle: 2FA (Two-Factor Authentication)

1. [ ] GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication). You sign in with something you know (your password) and something you have (a code you can copy from your Virtual Multi-Factor authentication Device, like FreeOTP, 1Password or Google Authenticator). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically").
    - If you have problems with 2FA just let IT Ops know in the #it_help Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling. It's better than getting locked out and needing to wait for IT Ops to help you get back online.

***Do the next step Today. This absolutely must be done within 48 hours or you will be locked out and need to wait for IT Ops to get around to help you get reconnected.***
1. [ ] :red_circle: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com
account.
**If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.**

### :red_circle: BambooHR

***This section is super important to ensure we can continue our tasks in the People Group and Payroll team. Please complete these items on your first day if at all possible.***

1. [ ] BambooHR is our HRIS (Human Resource Information System) for all team members. We have self-service enabled so that at any time you can access your account to see your employment information and documentation. As part of onboarding, please make sure to update everything that is applicable to you. The information from BambooHR is used to feed other systems, so it is important that it is filled out before you start.


**Our Total Rewards team usually enrolls new team members in all benefits on **Day 4** of onboarding (and roll back to ensure you are covered from day 1). If you are not enrolled by day 4, kindly send a slack message in the #people-connect Slack channel.**

   1. [ ] Access BambooHR from your Okta account dashboard.
   1. [ ] :red_circle: Please upload a scanned photo of your ID/Passport and Work Permit/Visa in BambooHR (My Info => Documents => Employee Uploads) by the latest of day 2 of your onboarding. If your ID documents have already been uploaded by People Experience (check the Verification Docs folder), please disregard this task. If no document(s) are there, please upload and notify People Experience by commenting at the bottom of this issue, tagging your People Experience Associate assigned to your issue by entering their GitLab handle and your message. Rest assured that this is only viewable to People Team Admins. Please note that completing this task late may result in the late processing of your first pay.
   1. [ ] On the Personal tab of the My Info page, enter your:
      1. [ ] :red_circle: Birth Date: This field is **mandatory** and is **confidential**, in order for Total Rewards team to complete employee verification and compliance. Rest assured that this is only viewable to People Team Admins. Please ensure your birth date has been filled out in the correct format. Incorrect details may result in the late processing of your first pay.
         - Once you have entered the date of birth, the field will grey out and can no longer be edited. Not to worry, a notification is sent to our Total Rewards team to approve the update. Once approved, you will be able to view your date of birth field in BambooHR.
**For US based team members, please note that it is not necessary for your middle name to be entered in the system**
      1. [ ] Name fields (representing Legal First Name, Legal Middle Name, Legal Last Name): Please confirm that these fields have been filled correctly with your complete Legal First, Middle and Last Name(s), as seen on any Government-issued ID. If the information needs to be updated, please ping your People Experience Associate (PEA) so they can update. 
      1. [ ] Preferred Name: Only fill out this field if you prefer to be known by a First Name other than your Legal First Name. Please leave the field blank if there is no change. Ex. If your Legal First Name is Emily but you prefer to be known as Emma, please enter Emma in the Preferred Name field.
      1. [ ] Preferred Last Name: Only fill out this field if you prefer to be known by the Last Name other than your Legal Last name. Please leave the field blank if there is no change. Ex. If your Legal Last name is Rodrigo de Leon Garcia but you prefer to be known with the last name Garcia, please enter Garcia in the Preferred Last Name field.
      1. [ ] National Identification Number in your current country of residence.
         - In the US, please format this as `###-##-####`.
         - In Germany, use your "Sozialversicherungsnummer" and select "Social Insurance Number" as type.
      1. [ ] Gender
      1. [ ] Other Gender Options (if applicable) (You can read more in our [Gender and Sexual Orientation Identity Definitions and FAQ](https://about.gitlab.com/handbook/people-group/gender-pronouns/) handbook page.)
      1. [ ] Marital Status
      1. [ ] Address
      1. [ ] Nationality
      1. [ ] Phone numbers (make sure to add the country code, i.e. all numbers should start with `+`)
      1. [ ] :red_circle: GitLab Username (this is the username you choose when setting up GitLab, not your GitLab email address) (**NB: Please copy this without the `@` symbol**)
      1. [ ] Family Members at GitLab (if applicable)
   1. [ ] Still on the My Info page, click Export Name/Location to Team Page. This task enables you to be added to our [team page](https://about.gitlab.com/company/team/). We are highly sensitive and inclusive to privacy. We therefore respect your decision to remain private/anonymous if you prefer. To add your entry to the [team page](https://about.gitlab.com/company/team/), please update the BambooHR field to "YES". This will trigger the entry automatically. If no action is taken or you enter "NO", an anonymized entry will be created. The intention behind the team page entries relates to our [Transparency value](https://about.gitlab.com/handbook/values/#transparency).
   1. [ ] On the Job tab, please scroll down to the Equal Employment Opportunity section, and enter your:
      1. [ ] Ethnicity. If you would prefer not to answer, please leave this field blank.
      1. [ ] Disability Status (Please read more about our [Disability Inclusion](https://about.gitlab.com/company/culture/inclusion/#disability-inclusion) in our handbook.)
   1. [ ] On the Emergency tab, enter in all emergency contact information.
   1. [ ] On the More tab, click Bank Information and fill in your bank details (Not applicable to USA, Safeguard, CXC & Global Upside team members).

### :red_circle: Payroll Info
1. [ ] :red_circle: Review Payroll Information for [US team members](https://about.gitlab.com/handbook/finance/payroll/#us) or [non US team members](https://about.gitlab.com/handbook/finance/payroll/#non-us) page (which ever is applicable to you) to acknowledge the various payroll and expense information.

### Acknowledgements

1. [ ] :red_circle: Review and sign the [Code of Conduct 2021](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d). This document was sent to your GitLab email inbox from BambooHR requesting your signature.
1. [ ] :red_circle: Review and sign the `Acknowledgement of Relocation 2021`. This document was sent to your GitLab email inbox from BambooHR requesting your signature.
1. [ ] :red_circle: Review and sign the `Consent to Collect and Use Data Regarding Race Ethnicity`. This document was sent to your GitLab email inbox from BambooHR requesting your signature. (Not applicable for team members located in France, Belgium, Denmark, Finland, Iceland, Norway, or Sweden)
1. [ ] :red_circle: Review and sign the `Social Media Policy Acknowledgment`. This document was sent to your GitLab email inbox from BambooHR requesting your signature.

If you have not received one or more of the above documents to sign, please comment in this issue and ping your assigned People Experience team member to resend.


### New Hire Swag

1. [ ] You would have received an email to order your new hire swag, if you have not done so already, take a break and complete your information by clicking the link in the email :smile:
     - Please note that in some instances, a customs fee may be applied at collection of the swag. If fees are charged, please be sure to expense the cost via Expensify for reimbursement i.e. you will need to provide the relevant receipt in order to do so.


</details>

---

### Day 2 - Remote Working and our Values

**Welcome to day 2 of onboarding! Today you'll work on compliance tasks, systems tasks, and use GitLab Learn. Your most important task today is to read about our values.**

<details>
<summary>New Team Member</summary>

### GitLab Learn

1. [ ] Today, you'll begin to explore training material in the GitLab Learn platform. GitLab Learn is our GitLab Learning Experience Platform for team members, customers, and community members. Get familiar with the platform by taking this [Getting Started on GitLab Learn training](https://gitlab.edcast.com/pathways/get-familiar-with-gitlab-learn) then review your assigned learning by visiting your `My Learning Plan` tab.
1. [ ] Set your Manager in EdCast, so you can receive assignments and track progress. To do this:
     1. Go to your [EdCast Dashboard](https://gitlab.edcast.com/me/learners-dashboard).
     1. Scroll down to the My Analytics > My Organization tile.
     1. Click "Add Manager"; search for your manager's name, select it, and confirm.
1. [ ] Complete this [Learning Style survey](https://forms.gle/SqrT1fCuE4VNfmyZA) and tell us how you prefer to learn so we can improve the GitLab Learn experience.

### Remote Work
1. [ ] The first month at a remote company can be hard. Particularly if you're new to working in a 100% remote environment, read over our [Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). You'll find other tips for operating (and thriving!) in an all-remote setting within the [All-Remote section of our handbook](https://about.gitlab.com/company/culture/all-remote/). Take some inspiration from our team members on how they structure their remote working day by reading this helpful blog post: [A day in the life of remote worker](https://about.gitlab.com/blog/2019/06/18/day-in-the-life-remote-worker/).
1. [ ] Learn more about the `#allremote` onboarding process and [best practices](https://about.gitlab.com/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
1. [ ] Start working to complete the [Remote Work Foundations Certification](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge) found in GitLab Learn. Focus on the first two assessments. These two lessons will set you up for success during your first few days at GitLab.

### Values
1. [ ] GitLab values are a living document. GitLab values are documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business. Familiarize yourself with our [Values page](https://about.gitlab.com/handbook/values/).
1. [ ] Take and pass our [Values Certification](https://gitlab.edcast.com/pathways/gitlab-values-badge) to become certified in our GitLab Values.

### Mission
1. [ ] Gitlab believes in a world where everyone can contribute. Take some time to [look over our mission](https://about.gitlab.com/company/mission/) to see how we plan to put this into action.

### Workspace <!-- Intern: Remove -->
1. [ ] Take some time to consider the setup of your workspace. Check out these helpful Handbook pages on how to [Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace). Having an ergonomic and comfortable workspace will enable the utmost productivity.
1. [ ] Review the [Spending Company Money Guidelines](https://about.gitlab.com/handbook/spending-company-money/#guidelines) and the [Office Equipment and Supplies Averages](https://about.gitlab.com/handbook/finance/expenses/#office-equipment-and-supplies).
1. [ ] You are able to [expense](https://about.gitlab.com/handbook/finance/expenses/#-reimbursements) your monthly Internet subscription. If you have a total cost package with your provider (telephone, Internet, cable), be sure to calculate only the cost of the Internet portion and expense that.

### Communication
1. Clear, considerate communication is especially important at an all-remote company. Next, we will review two more assessments within the [Remote Work Foundation Certificate](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge).
   - [ ] [Embracing asynchronous communication](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge)
   - [ ] [Communicating effectively and responsibly through text](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge)
1. [ ] Familiarize yourself with our [Communication page](https://about.gitlab.com/handbook/communication/).

### GitLab Gmail
1. [ ] Link your GitLab email address to an easily recognizable photo of yourself. It is company policy to use a photo. Do not use an avatar, stock photo, or something with sunglasses for your GitLab accounts.  We have a lot of GitLab team members and our brains are comfortable with recognizing people; let's use them.
1. [ ] Consider setting up an email signature. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#email-signature). Feel free to customize it how you'd like.

### Zoom
1. [ ] Set up your [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/zoom/#zoom-setup). Your Zoom account will be set up via [Okta for SSO Login](https://gitlab.okta.com/app/UserHome), so log in using the Zoom SSO tile.
1. [ ] Fill in your [profile](https://zoom.us/profile) with your Name, Job Title and Profile Picture. Since Zoom doesn't display the job title field during meetings, it is recommended that you [add your job title](https://about.gitlab.com/handbook/tools-and-tips/zoom/#adding-your-title-to-your-name) as part of the display name field. For example, if your name is `John Appleseed` and your role is `Engineer`, you can write display name: `John Appleseed - Engineer`.
   1. [ ] Consider adding your pronouns in the `Pronouns` field. By making it normal to set pronouns we create a more inclusive environment for people who use non-traditional pronouns.
      1. Below this, there is `How would you like to share your pronouns?`. Select the option that works best for you.
      1. Click `Save` and your pronouns should now be visible on Zoom.
1. Consider setting your default recording view to "Gallery view". To do this:
   1. Go to zoom.us and log into [your profile](https://zoom.us/profile).
   1. Click the `Settings` tab on the left, then the `Recording` tab.
   1. Make sure you have `Record gallery view with shared screen` selected, and unselect `Record active speaker with shared screen` and `Record active speaker, gallery view and shared screen separately`. Remember to save.
   1. _Mac only_ - in case you are not able to share the screen in Zoom desktop client (as it is disabled by default), check this link [Permissions for the Zoom desktop client](https://support.zoom.us/hc/en-us/articles/360016688031) how to fix the issue.
1. [ ] Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.

### Calendar

#### Google Calendar Set Up
1. [ ] Read our handbook section on [Google Calendar](https://about.gitlab.com/handbook/tools-and-tips/#google-calendar)
1. [ ] Set your Google Calendar [default event duration](https://calendar.google.com/calendar/r/settings) to use `speedy meetings`
1. [ ] Verify you have access to view the GitLab Team Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to  calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com` and then press enter on your keyboard. Reach out to a People Experience Associate if you have any questions. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar.
1. [ ] Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings. However, even private meetings are viewable by your manager and those who manage them.
1. [ ] [Set your working hours & availability](https://support.google.com/calendar/answer/7638168?hl=en) in your Google Calendar.

#### Calendly
1. [ ] Set up [Calendly](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#sts=Calendly). Calendly is a calendar tool that allows individuals to select open meeting slots in order to speak with others outside of GitLab. For GitLab team members, feel free to [schedule directly in Google Calendar](https://about.gitlab.com/handbook/communication/#scheduling-meetings). When you are setting up Calendly, there is no specific GitLab account. With the free version you will only be allowed one meeting time, but if you need to upgrade to a Pro account, you can do so and expense it per [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [ ] Add the Zoom integration by going to the `Integrations` section at the top of the page. Find Zoom in the list of integrations and click it. It will then ask you to link your Calendly and Zoom accounts together. Do so.
1. [ ] Add your Calendly link to your Slack Profile.

#### Events
Your calendar includes invitations to the following meetings. They are optional, but you are highly encouraged to attend. Please note: These meetings may not fall during your first week.

1. [ ] If you have questions getting your work station set up, try to sign up within 2 weeks for [Gitlab IT Onboarding/101](https://calendar.google.com/calendar/u/0/r/eventedit/NGY5ODg2ZzRoZWFnZDQ2NWx2MDFwcjZ0M2IgZ2l0bGFiLmNvbV82ZWtiazhmZnFua3VzM3FwajlvMjZycWVqZ0Bn?tab=mc). This is an optional meeting for you to ask questions. We run this live troubleshooting session weekly, so you can always drop into a later meeting.
1. [ ] On the GitLab Team Calendar, watch for the Security Group Conversation, Show & Tell, and Security Office Hours. During these Zoom calls, you can ask any security-related questions and see updates from the Security team. Join the #security Slack channel to ask any questions you may have asynchronously.
1. [ ] In the [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/) calendar description, find the call agenda link where all team members can insert questions for the CEO. Tip: Videos of previous CEO 101 sessions within the GitLab archives can help you determine questions to contribute.

### Your Role
- Spend the rest of your day focused on Role-based tasks. You'll find job-specific tasks at the end of this issue and in your assigned role-based onboarding issue as applicable.

</details>

---

### Day 3 - Security & Compliance

**Welcome to day 3! By now, you should have your systems in place, more familiarity with our values, and have accessed GitLab Learn. Today, you will be going over Security and the remaining compliance pieces of onboarding.**

<details>
<summary>New Team Member</summary>

### :red_circle: Security

1. [ ] :red_circle: Read through some of our key policies, procedures and documents from the GitLab Control Framework.
   1. [ ] [Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
   1. [ ] [Incident Response Guidance](https://about.gitlab.com/handbook/engineering/security/vulnerability_management/incident-response-guidance.html)
   1. [ ] [Information Security Policies](https://about.gitlab.com/handbook/engineering/security/#information-security-policies)
   1. [ ] [Record Retention Policy](https://about.gitlab.com/handbook/legal/record-retention-policy/)
1. [ ] :red_circle: Encrypt your hard drive. Instructions for Apple can be found [here](https://about.gitlab.com/handbook/security/#laptop-or-desktop-system-configuration) with additional information around the Linux set-up process [here](https://about.gitlab.com/handbook/tools-and-tips/linux/#initial-installation).
1. [ ] :red_circle:  Leave a comment in this issue with a screenshot to verify that your hard drive is encrypted. Be sure to include the serial number of your computer for correlation.
   1. [ ] Please review the [Full Disk Encryption](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#full-disk-encryption) page in the handbook for the appropriate steps to create the screenshot with the necessary information.
   1. [ ] To take a screenshot on an Apple machine press `Command` + `Shift` + `3` to take a screenshot of the entire desktop, alternatively `Command` + `Shift` + `4` will take a screenshot of the area you select. If you are using a Linux machine `Prt Scrn` will take a screenshot of the entire desktop; `Alt + Prt Scrn` will take a screenshot of a specific window and `Shift + Prt Scrn` will take a screenshot of an area you select.
   1. [ ] To Upload the screenshot to your issue: Find the `Attach a file` link at the bottom of this issue and press Comment. Depending on the operating system you are using, the steps to retrieve this information may be different.
***Please note that GitLab's [Internal Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#unacceptable-system-and-network-activities) prohibits the use of tools that capture and share screenshots to hosted third-party sites so please make sure that you do not leverage such a tool to complete this step.***
1. [ ] Please add your serial code for your GitLab laptop here: [GitLab Notebook information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform?usp=sf_link). You will need the evidence of full disk encryption from the previous step.
1. [ ] Review the link to our internal [Phishing Program documentation](https://about.gitlab.com/handbook/engineering/security/security-assurance/governance/phishing.html) 

### :red_circle: Legal
1. [ ] :red_circle: Set-Up your [NAVEX Global](http://gitlab.lms.navexglobal.com) account and complete the trainings listed in there. You will be able to log into NAVEX via Okta. If you experience any issues, reach out to the team in the Slack channel `#compliance-training`.
1. [ ] :red_circle: Complete [Regulation FD training](https://gitlab.edcast.com/pathways/gitlab-regulation-fd-training) in GitLab Learn.
1. [ ] :red_circle: Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/)
1. [ ] :red_circle: Review [GitLab's Anti Bribery and Corruption policy](https://about.gitlab.com/handbook/legal/anti-corruption-policy/). Mark this task as complete to acknowledge that you have read, understand and agree to abide by the policy.
1. [ ] :red_circle: Don't forget to comply with the contract you signed, and make sure you understand [confidentiality](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d) (see page 6 of the Code of Business Conduct and Ethics).

### :red_circle: Compliance
1. [ ] Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/). Pay particular attention to any country-specific requirements that relate to your location.
1. [ ] Please complete the [Harassment Prevention Training](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive) via WILL Learning within your first 30 days. If you did not receive an email invite, review the instructions and find the appropriate course link [on this handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive).
   1. [ ] **Important Note (Trigger Warning):** GitLab is sensitive to those who may have experienced harassment in the workplace previously and would like to remind new team members of the Employee Assistance Program we offer through [Modern Health](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/) if you would like someone to talk to. 
   1. [ ] Ensure you upload your completion certificate into BambooHR, following the instructions [on this handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive).
1. [ ] Review and familiarize yourself with our [SAFE Framework.](https://about.gitlab.com/handbook/legal/safe-framework/)
1. [ ] Please read our [People Policies](https://about.gitlab.com/handbook/people-policies/) page. This contains important team member-related policies to ensure that we adhere with all required laws.
1. [ ] Please review and read any [Labor and Employment Notices](https://about.gitlab.com/handbook/labor-and-employment-notices/) that apply to your location.



### Your Role

- Spend the rest of your day doing your Role-based tasks. See the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable. 

</details>

---

### Day 4 - Social & Benefits

**Welcome to Day 4! This day brings you to the social aspects of GitLab, as well as learning about and enrolling in benefits. You will learn about the team, how we connect to one another, and our benefits.**

<details>
<summary>New Team Member</summary>

### Get to Know the Team

1. [ ] Review the GitLab Organizational Chart. You can view the chart and structure [in the handbook](https://about.gitlab.com/company/team/structure/) or by going to BambooHR, then clicking on People, and Organization Chart.

**Coffee Chats** 
[Coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) are how we get to know our team members. They are informal and casual. 

During your first two weeks, schedule 5 calls with different team members. 

There's no need to ask people if it's okay to schedule a call first. Schedule a meeting using the [Find a time](https://about.gitlab.com/handbook/tools-and-tips/#finding-a-time) feature in Google Calendar. Be sure to introduce yourself in the invite.

Not sure where to start connecting? Below are some ideas to get you started.

- You can view GitLab team members by role and team on the [team organitzation chart](https://about.gitlab.com/team/chart/) and where each team member currently is in the world via the [team page](https://about.gitlab.com/company/team/). 
- In support of GitLab's values of [Diversity, Inclusion & Belonging](https://about.gitlab.com/company/culture/inclusion/) scheduling calls with team members in other regions and countries.
- See who engaged with your introduction in Slack in [#new_team_members](https://gitlab.slack.com/messages/new_team_members/).
- Ask your onboarding buddy or your manager.
- Consider joining the [Let's Connect - GitLab Social Call](https://about.gitlab.com/handbook/communication/#social-call).
- Check out the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week.
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___

**Note:** There's no need to ask people if it's okay to schedule a call first – go ahead and schedule a meeting using the [Find a time](https://about.gitlab.com/handbook/tools-and-tips/#finding-a-time) feature in Google Calendar and introduce yourself in the invitation. 

We also recommend "Closed Captioning" and "Live Transcription" are enabled in  [Zoom account settings](https://zoom.us/profile/setting).

### Social Media and Connecting
**GitLab Profile**
1. [ ] Fill in your [GitLab.com profile](https://gitlab.com/-/profile), including: Organization = "GitLab", Bio = "(title)".
   1. [ ] Given our value of [transparency](https://about.gitlab.com/handbook/values/#transparency) and because GitLab is [public by default](https://about.gitlab.com/handbook/values/#public-by-default), ensure you do *not* use a [private profile](https://docs.gitlab.com/ee/user/profile/#private-profile), i.e. make sure that the checkbox under **Private profile** is unchecked.
   1. [ ] Consider adding your pronouns to the `Pronouns` field. This creates a more inclusive environment for people who use non-traditional pronouns.
      1. Click the `Update Profile Settings` button at the bottom of the page to save these changes.
      1. Your pronouns will now be visible on your profile and when you hover over any @mentions.
   1. [ ] Consider adding your pronunciation guide to the `Pronunciation` field for your full name to help others to pronounce your name correctly (e.g. sid see-brandy for Sid Sijbrandij).

**GitLab Social Media**
1. [ ] Connect with or follow GitLab's social media sites: [LinkedIn](https://www.linkedin.com/company/gitlab-com), [Twitter](https://twitter.com/gitlab), [Facebook](https://www.facebook.com/gitlab), and [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).

**GitLab Unfiltered**
1. [ ] Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. This is super important, ***if you do not accept this invite, you will not be able to watch the recordings on Youtube Unfiltered and we use this platform daily***. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
    1. When you accept the invite, you will see “GitLab Unfiltered” under “Your Brand Accounts” section.
    1. Please do not edit or change permissions of the “GitLab Unfiltered” account. This is a shared account for the company maintained by Marketing.
    1. To view GitLab Unfiltered content:
         1. Open a new browser tab and navigate to [YouTube](https://www.youtube.com) 
         1. Ensure that you have **switched** your account to the GitLab Unfiltered profile
         1. Not sure if you have? Watch the[Unable to view a video on Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube)
    1. Confirm you have completed the above task, by watching [TEST Private Stream](https://youtu.be/gCyUdHixHN0).
    1. Unable to view the video? Go back to the above steps.
    1. After watching videos using the "GitLab Unfiltered" account, **switch back** to your own account. 
   
**Moo Business Cards**
1. [ ] You will receive an email inviting you to Moo. Moo is the system we use to place orders for [business cards](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards).
**_note_** you will purchase the cards using a personal bank account, and then submit the expense for reimbursement. 

### Benefits
1. [ ] If you are employed by one of our [entities](https://about.gitlab.com/handbook/people-group/employment-solutions/), view your entity's [benefits information](https://about.gitlab.com/handbook/total-rewards/benefits/).
1. [ ] If you are not already offered a GitLab life insurance plan through the [entity-specific benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/), fill in the [expression of wishes form](https://docs.google.com/document/d/19p4IN-msvs4H10teBessfPx-1wu8J7Vs67Es8iVLlL8/edit?usp=sharing). For your beneficiaries to have access to the benefit, you must indicate in writing to whom the money should be transferred. To do this, copy the template to your Google Drive (File > Make a copy), enter your information, and sign electronically. To sign the document, you can use one of the following methods:
   - Print, sign and digitize; or
   - Export to PDF (File > Download > PDF Document (.pdf)), and use one of the following tools to electronically sign the exported document:
     - In macOS, [use the native Preview app](https://support.apple.com/guide/preview/fill-out-and-sign-pdf-forms-prvw35725/mac).
     - In Linux, [use the Xournal app](https://www.xmodulo.com/add-signature-pdf-document-linux.html)'s [pen tool](http://xournal.sourceforge.net/manual.html#tools) to draw the signature.
     - Use an online tool like [eSign PDF](https://smallpdf.com/sign-pdf) (requires uploading the file) or [HelloSign](https://app.hellosign.com/) (requires an account). Please beware that there are always security concerns with tools that require uploading personal data. So, you may want to do this as a last resort only.
   - After signing, upload the PDF file to the **Employee Uploads** folder in BambooHR.

**Time Off**
1. [ ] Review our "no ask, must tell" [Paid time off policy](https://about.gitlab.com/handbook/paid-time-off).
- GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. 
- If you feel uncomfortable about taking time off, feel free to reach out to your manager or People Experience. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
1. [ ] Complete the [PTO training](https://gitlab.edcast.com/pathways/pto-training-april) in EdCast.

**Mental Health Benefits**
1. [ ] GitLab offers an Employee Assistance Program to all team members, through Modern Health. Read more in the [Modern Health handbook page](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/) for more information on this amazing benefit. Your account may not be immediately available but will be within the first 14 days. New GitLab team members are included in weekly waves to Modern Health.
1. [ ] At GitLab, we strive to create a safe space for all team members, including those with mental health predispositions or neurological differences. Consider joining the `mental_health_aware`, `neurodiversity`, and/or `diverse_ability` Slack channels for more awareness/support on these topics.

**Equity & Compensation**
1. [ ] Read about GitLab [Equity compensation](https://about.gitlab.com/handbook/stock-options/). If you received restricted stock units as part of your contract, your RSU's will be approved by the Board of Directors at their [quarterly board meetings](https://about.gitlab.com/handbook/board-meetings/#board-meeting-schedule).  After your grant has been approved by the Board, you will receive a grant notice by email and access to Carta.
1. [ ] Take the Following Assessments (remember any questions you may have around these can be routed to the #people-connect Slack channel):
   - [ ] [GitLab Compensation Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/compensation/#knowledge-assessment)
   - [ ] [GitLab Benefits Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/benefits/#knowledge-assessment) 

### Payroll & Expensing
1. [ ] Take a look at the [Accounts Payable](https://about.gitlab.com/handbook/finance/accounts-payable/) page for information on how to expense items and sending invoices
1. [ ] Keep track of your expenses in Expensify (via Okta). If you have any questions on submitting an expense report ask in the `#expense-reporting-inquires` slack channel.
1. [ ] Take a look at the [Payroll](https://about.gitlab.com/handbook/finance/payroll/) page for more information on paychecks.

### Travel & Contribute
1. [ ] Check the [GitLab Contribute handbook page](https://about.gitlab.com/company/culture/contribute/) to get familiar with the GitLab Contribute event.
1. [ ] View the `Card 4.19.17.pdf` located [here](https://drive.google.com/file/d/0B4eFM43gu7VPOHlLWFNBV1BZVE0tZ2NDVDJQd0hBWVotUGIw/view?usp=sharing&resourcekey=0-rruqO9weBMej2mDaDhBFZg) and save your [Traveler Insurance ID card](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy) located on page 1.
1. [ ] We use TripActions for company travel; please [create your account through Okta](https://gitlab.okta.com/app/UserHome). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to setup your account.

### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

</details>

---

### Day 5 - Git

**Welcome to day 5! Today you will start learning more about Git and how we use it in every day work. Do not be intimidated, we have some great resources for learning. Please do keep in mind that it may take you longer than a day to complete this section, that is okay!**

<details>
<summary>New Team Member</summary>

For those new to Git and GitLab, it's important to get familiar with how git works. You're not expected to become a GitLab master in a day, but to understand that the below is a key reference as you acclimate. Be sure to reach out to your onboarding buddy, manager, or any team member at GitLab for Git-related questions and help.

##### Helpful Slack Channels for Git Questions
- Any help with Merge Requests: [`#mr-buddies`](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/)
- Any general help with Git: [`#questions`](https://gitlab.slack.com/archives/questions)
- Specific questions using Git in the terminal: [`#git-help`](https://gitlab.slack.com/archives/git-help)

##### Introduction to Git and GitLab - SSH Access (familiar with Git)

1. [ ] If you have never used Git before, we **recommend** you skip this step and use the [WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/) to complete any Git-related onboarding tasks. The WebIDE doesn't require you to set or use any command-line tools.
   * If you plan to use Git on your local machine then you'll need to [add an SSH key to your GitLab profile](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account). The SSH key provides security and allows you to authenticate to the GitLab remote server without supplying your username or password each time. Please note that following best practices, you should always favor ED25519 SSH keys, since they are more secure and have better performance over the other types.
   * Alternatively, you can clone over HTTPS. You'll need to generate a [personal access token](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) as account password with 2FA enabled. This method helps when SSH is blocked in your environment (e.g. hotel wifis).
   * Some GitLab projects (for example [Version App](https://gitlab.com/gitlab-services/version-gitlab-com#contributing)) require commits to be signed with GPG. Consider [setting up GPG Key](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#generating-a-gpg-key).

##### Introduction to Git and GitLab - General Breakdown

1. [ ] Complete the [GitLab Team Members Certificaton](https://gitlab.edcast.com/pathways/gitlab-team-members-certification-this). This certification will take approximately 6 hours to complete and will teach the fundamentals of using GitLab for team members. Consider blocking off time during your onboarding to complete
1. [ ] Become familiar with how Git works by watching this [Git-ing started with Git video.](https://www.youtube.com/watch?v=Ce5nz5n41z4)




##### Merge Requests

Note: Many of the tasks below are covered in the GitLab Team Members Certification. Please review the items below as an additional refresher on Merge Requests. 

1. [ ] When you are first starting at GitLab and creating new merge requests, you can assign the MRs to your manager. Going forward, it's best practice to assign the MR to the relevant department. For example, if you are making an edit to the People Experience section of the handbook, please assign it to someone within People Experience. If you're unsure of who to assign a merge request to or if you have any questions or problems, feel free to reach out to your manager or onboarding buddy. When you join GitLab your GitLab.com account will be added to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a Developer. Please see this [blog post discussing how we assign permissions](https://about.gitlab.com/2014/11/26/keeping-your-code-protected/), as well as a [permission levels chart](https://docs.gitlab.com/ee/user/permissions.html) to find out more about what each level can do.
1. [ ] Review best practices for issues and merge requests. It is important that we make iterations quickly and efficiently. Do not wait until you are done before creating an issue or merge request. Making issues and merge requests quickly and in small iterations allows the information to be quickly updated in the handbook and available for review by everyone. It also reduces the risk of changes becoming lost or out of date.
   - `Draft:` is what you prepend to a merge request in GitLab in order to prevent your work from being merged if it still needs more work. When you are working on a big issue, create a merge request after the very first commit and give it a title, starting with `Draft:`, so you can iterate on it. For more information regarding issues, merge requests, and overall best practices regarding communication, please review [the communication guidelines](https://about.gitlab.com/handbook/communication/).
1. [ ] Review our [Practical Handbook Edit Examples](https://about.gitlab.com/handbook/practical-handbook-edits/) to learn more about creating new handbook pages and multimedia embedding best-practices, changing a page name and subsequent updates, creating mermaid diagrams, creating issue templates, among others. If you run into any issues while following those instructions, post a question in [`#mr-buddies`](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/) for someone to look at and improve the documentation.
1. [ ] Review the [guidelines on how to change or update a process](https://about.gitlab.com/handbook/handbook-usage/#how-to-change-or-define-a-process) specifically the content related to `CODEOWNERS` and selecting a suitable title for your Merge Requests.
1. [ ] Make an improvement to the [handbook](https://about.gitlab.com/handbook/), [general onboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md), or your departments' onboarding issue. Here at GitLab, everyone contributes to our way of working in this way. Being new, you may be unsure if your idea for a change is good or not, and that's OK! Your merge request starts the discussion, so don't be afraid to offer your perspective as a new team member.
   - [ ] Handbook: If your onboarding buddy has merge rights, assign the merge request (MR) to your onboarding buddy. If your onboarding buddy does not have merge rights, assign to your manager. Ping them in the [#new_team_members](https://gitlab.slack.com/messages/new_team_members/) slack channel, and link the MR URL in this onboarding issue. When you are making changes in the handbook, you are making changes in the www-gitlab-com project. If your manager does not have access to merge your MR based on the permissions described above, then please reference the specific [members page](https://gitlab.com/gitlab-com/www-gitlab-com/-/project_members?page=3&sort=access_level_desc) for the www-gitlab-com project. 
   - [ ] Onboarding issue: If the issue template is related to your department, assign it to your manager. Otherwise, for the general onboarding issue mention your People Experience Associate (PEA) as a comment in the MR.
1. [ ] Learn more about GitLab quick actions and keyboard shortcuts for issues and merge requests in [this blog post on productive workflows](https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/)

It is highly encouraged to complete the [GitLab Team Members Certificaton](https://gitlab.edcast.com/pathways/gitlab-team-members-certification-this) before moving onto the **[Team Page](https://about.gitlab.com/company/team/)** steps.

##### Adding Yourself to the [Team Page](https://about.gitlab.com/company/team/)

Please be aware that this task is a key learning opportunity and strongly recommended for all team members. This exercise is an important step as a GitLab team member and is a critical first example of learning our product firsthand, in a personalized way.

1. [ ] Update your photo, social media handles and story on the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). If there is anything that you would prefer not to have added to the team page, such as your exact location, feel free to use a broad location or remove this entirely.
   * If you are a manager, your slug will need to match the `reports_to` line for the people on your team. If you update your slug you should update `reports_to` for your team members as well.
   * If you are in the Sales organization, please consider adding your Sales region.
   * Please make sure to include your nickname if you prefer to be called anything other than your first name. If you do not feel comfortable with your full name on the team page, please change it to what feels appropriate to you.
1. [ ] If you have a pet, please also consider adding them to the [team pets page](https://about.gitlab.com/team-pets/) by following the instructions on the [same page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). Please ensure to add your first and last name and/or GitLab handle so that we can identify whose pets belong to whom.

**Please note in the current YouTube video, at 10:17 mark, you will need to use the `sites/uncategorized/` rather than `sites/marketing/`**

#### Weeks 2-4 Reminders
- [ ] Schedule work blocks over the next 30 days on your calendar to complete the tasks under Weeks 2 - 4 below. [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays) are a great time to come back to this issue.

#### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

#### Happy Dance

1. [ ] Do a little happy dance. You just finished your first week at GitLab!

**This week was focused on getting compliance completed, getting your systems in place, enrolling in benefits, and learning more about life at GitLab. Next week, is also very important in your onboarding here at GitLab. You will take a deeper dive into our core Values and handbook and most importantly learn about the importance of Diversity, Inclusion, and Belonging here at GitLab. We hope you have enjoyed your first week!**

</details>

---

### Weeks 2 - 4 : Explore <!-- Intern: Adjust to second week -->

**Welcome to week two :tada: This week really emphasizes the important of diveristy, inclusion, and belonging. This is something that is very important here at GitLab. This week is also great for continuing to read the handbook, learning how to use Git, and to keep getting to know your team.**

<details>
<summary>New Team Member</summary>

1. [ ]  Be sure to spend equal time on this critical onboarding issue, as well as your role-based onboarding and training.
1. [ ] :red_circle: Put some time aside to uncover new information in the team [handbook](https://about.gitlab.com/handbook/). A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. This is NOT a traditional company handbook of rules and regulations. It is a living document that is public to the world and constantly evolving. Absolutely anyone, including you, can suggest improvements or changes to the handbook! It's [encouraged](https://about.gitlab.com/handbook/handbook-usage/)! The handbook may seem overwhelming, but don't let it scare you. To simplify navigating the handbook, here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.
   - [ ] Read [about](https://about.gitlab.com/about/) the company, and [how we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](https://about.gitlab.com/culture/), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).

#### Diversity, Inclusion & Belonging
1. [ ] Understanding that Diversity, Inclusion & Belonging is one of GitLab's top priorities please complete GitLab's [Diversity, Inclusion & Belonging certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05)
1. [ ] The following resources were covered in the DIB certification above. Please acknowledge that you have gone through these pages. **Note:** You do not need to re-read.
   - [ ] GitLab's [Diversity, Inclusion & Belonging page](https://about.gitlab.com/company/culture/inclusion/)
   - [ ] GitLab encourages team member participation in [Team Member Resource Groups (TMRGs)](https://about.gitlab.com/company/culture/inclusion/erg-guide/) which include [Minorities in Tech (MIT)](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/), [Women+](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/) and [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) amongst [others](https://about.gitlab.com/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels).
   - [ ] GitLab's [Diversity, Inclusion & Belonging Events](https://about.gitlab.com/company/culture/inclusion/diversity-and-inclusion-events/)
   - [ ] GitLab's [Diversity, Inclusion & Belonging trainings](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities)
   - [ ] GitLab's [Building an Inclusive Remote Culture page](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/)
   - [ ] Optionally, you can contribute by collaborating or creating a new [issue](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues)

#### Additional Product Information
1. [ ] GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
1. [ ] Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
1. [ ] Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/#three-tiers) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motivation behind each tier.

#### Surveys and Reviews
1. [ ] Set a reminder for 30 days past your start date to complete the [Onboarding Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) and kindly review [the OSAT](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat). It is vitally important for us to continue getting feedback and iterate on onboarding.
1. [ ] If you feel comfortable, please create a video sharing your onboarding experience as per this [process](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/). This will definitely be helpful to all future new team members!
1. [ ] Help spread the word about life at GitLab. Set a reminder in sixty days to share your feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab). We welcome positive and negative feedback.

</details>

### Job-specific tasks

---
<!-- include: role_tasks -->

<!-- include: people_manager_tasks -->

<!-- include: division_tasks -->

<!-- include: intern_tasks -->

---

<!-- include: department_tasks -->

---

