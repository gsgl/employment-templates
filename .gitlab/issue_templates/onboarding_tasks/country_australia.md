### Day 1 - For Team Members in Australia only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please provide the following to nonuspayroll@gitlab.com: [Tax file number declaration](https://www.ato.gov.au/uploadedFiles/Content/IND/Downloads/TFN_declaration_form_N3092.pdf) and Superannuation information.
1. [ ] New team member: Add your bank details in BambooHR (this must be done by day 1 or latest day 2).
1. [ ] New team member: [Review Fair Work Information Statement](https://drive.google.com/file/d/1EP_pgKtSeMwtHeuKOFZfkkmFT2jI9W85/view?usp=sharing)
1. [ ] New team member: Review [Remote Work Checklist](https://docs.google.com/document/d/1_sHk3ksGLDVxBZsnO3pMD-U_R_Fy0Yyu/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true). Mark this item as done to confirm that your workspace and work environment meet all standards listed on the checklist and to acknowledge that you know to reach out to [Team Member Relations](mailto:teammemberrelations@gitlab.com) to discuss health and safety accommodations that may be needed and/or report any incidents. 

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>

<details>
<summary>Legal</summary>

1. [ ] @tnix Confirm that team member has acknowledged receipt/review of remote work checklist. 

</details>
