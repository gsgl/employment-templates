### Day 1 - For Team Members in Belgium Only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: To ensure that we have all of your details ready for payroll processing please complete the following information on BambooHR:
     1. [ ] Full name
     1. [ ] Date of birth
     1. [ ] Address
     1. [ ] National Number
     1. [ ] Bank details
1. [ ] New Team Member: Please read the Work Rules in either [Dutch](https://drive.google.com/file/d/1o_7sbqqxlxZW8PksXblgmbs8gi4yPrvb/view?usp=sharing) or [French](https://drive.google.com/file/d/13geOdZuMCE4XX8e5ejd5Qe5FAMnR1gWC/view?usp=sharing).
1. [ ] New Team Member: Be sure to upload your vacation certificate from your previous employer to the `Employee Uploads` folder BambooHR. Please notify your People Experience Associate in this issue when it has been uploaded to ensure processing of vacation pay in June.
</details>


<details>
<summary>People Experience</summary>

### Before start date:

1. [ ] People Experience: Open the [Belgium New Hire Form](https://docs.google.com/spreadsheets/d/1DEWQmhs4AVjRJUv_1L2iS8k7Ynl_IxBE/edit#gid=1440831444) in Google Drive. Select `make a copy` and save in the new team members name. Share the form with the team members personal email address as well as `nonuspayroll @gitlab.com`.
1. [ ] People Experience: Email the new team member the [Belgium email](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/email_templates/belgium_email.md) and cc `nonuspayroll @gitlab.com`.

### After start date:
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`
1. [ ] People Experience: Check that the team member has uploaded the Vacation certificate from previous employer to BambooHR `Employee Uploads` folder. 


</details>
