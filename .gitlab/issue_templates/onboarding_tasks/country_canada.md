### Day 1 - For team members in Canada only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Please read the [Canada Corp Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits/) handbook page.
1. [ ] New Team Member: If you wish to enroll in the [Registered Retirement Savings Plan (RRSP)](Registered Retirement Savings Plan (RRSP)), please use the [instructions in the handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits/#enrollment-1) to sign up through Canada Life.
1. [ ] New Team Member: FYI _no action required_, the Payroll team will send your payment information to our payroll provider as soon as you are added to our HRIS (BambooHR).

</details>

<details>
<summary>Total Rewards</summary>

1. [ ] Total Rewards Analyst (@julie.samson): Add team member to benefits platform, [Collage](https://www.collage.co/).

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll (@sszepietowska): Add new team member to Canada payroll and send invitation to team member.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>
