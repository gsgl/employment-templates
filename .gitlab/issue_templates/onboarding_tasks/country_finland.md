### For Team Members in Finland

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Please ensure to upload a copy of your passport and bank card/details to the Employee Uploads section in BambooHR. This ensures that we get everything set up for your payroll. Please ping your respective People Experience Associate in the issue once complete. 
1. [ ] Please complete the [Finland - New Employee Information Sheet](https://docs.google.com/spreadsheets/d/1tOYN9ylajqEr-Q_MmHs4Imu8j1QXuJ7V/edit#gid=1413931868) > click file, make a copy and save in your name (please ensure to duplicate otherwise your personal information can be obtained by other team members).

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Send HR Savvy the following documentation (password protected) in order to process payroll and set up of team member
    - Signed contract
    - Copy of passport
    - Proof of bank card/details
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the respective People Experience Associate in the onboarding audit rotation. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Once the team member has completed the Finland - New Employee Information Sheet, download as PDF and save to Payroll folder in BambooHR.
1. [ ] People Experience: In the team member's onboarding issue, ping the Non-US Payroll team members `@nprecilla` and `@sszepietowska` letting them know that the Payroll form has been completed and in the Payroll Forms folder. 

</details>
