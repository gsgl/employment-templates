### For team members in France

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Download the following documents from [Egnyte](https://globalupside.egnyte.com/app/index.do#storage/files/1/Shared/Clients/GU/Gitlab/Onboarding%20Documents) (Global Upside's online portal):
    - Employee Personal Particulars Form
    - Bank Details Form
    - Consent form 
    - Id/social security cert (front & back)
    - Bank letter

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `1 to 3-month Probationary Period until YYYY-MM-DD (relevant months after hire date)` *See contract for exact time frame
   * Effective Date: `1-3 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `1-3 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Check all required payroll documents have been sent, completed and uploaded to BambooHR.
1. [ ] People Experience: In the team member's onboarding issue, ping the Non-US Payroll team member `@nprecilla` and `@sszepietowska` letting them know that the Payroll questionnaire is completed and in the Payroll Forms folder. 
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>

<details>
<summary>Team Member</summary>

1. [ ] :red_circle: Team Member: Review the GitLab France S.A.S. [Remote Work Policy](https://docs.google.com/document/d/1_8-m3QVNXIN7idfv-F0eYJqRhipjrsWx/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true).

</details>
