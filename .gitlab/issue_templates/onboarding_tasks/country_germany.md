### For Team Members in Germany

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Read through the GitLab GmbH benefits [page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany/). 
1. [ ] New Team Member: Review [Work from Home Checklist](https://docs.google.com/document/d/1Z3i-vrkcU5ald0j-scf2rgTumwFCc_sI/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true). Mark this item as done to confirm that your workspace and work environment meet all standards listed on the checklist and to acknowledge that you know to reach out to [Team Member Relations](teammemberrelations@gitlab.com) to discuss health and safety accommodations that may be needed and/or report any incidents.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Open the [GitLab GmbH Employee Payroll Form](https://docs.google.com/spreadsheets/d/1gBVDpk_mk2iGyfcrLEEhfUGtoMzDC3tb/edit#gid=1185395756) in Google Drive. Select `make a copy` and save in the new team members name. Share the form with the team members personal email address as well as `nonuspayroll @gitlab.com`.
1. [ ] People Experience: Email the new team member the [Germany email](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/email_templates/germany_email.md) and cc `nonuspayroll @gitlab.com`.
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>

<details>
<summary>Legal</summary>
1. [ ] @tnix Stage Work from Home Checklist for signature in docusign.

</details>
