### Day 1 - For Team Members in India only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read through the [India Specific Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india/). This will explain what is available. If you have any questions please contact Global Upside at hr@globalpeoservices.com (Employment-related questions) or benefitsops@globalupside.com (Questions regarding benefits elections). You may also be contacted by Global Upside during your first week of starting at GitLab to complete their onboarding documents for payroll (if this has not already been done during the contract signing stage).
1. [ ] New team member: Global Upside will provide you with a declaration form on which you will indicate your desired Employees Provident Fund (EPF) contribution i.e. against assumed compensation or actual compensation.  It should be noted that Global Upside is not able to request or process a **reduction** in EPF contribution once your selection has been made.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>
