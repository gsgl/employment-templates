### For team members in Ireland

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>

<details>

<summary>Team Member</summary>

1. [ ] Team Member: Review the [Ireland Benefits page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-ireland-ltd/) to enroll in medical, pension, etc. 

1. [ ] Team Member: [Complete Home Working Checklist](https://forms.gle/1U6nabJkYh8xQXdk7) within first 30 days of start date.

</details>

<details>

<summary>Total Rewards</summary>

1. [ ] Total Rewards @julie.samson: To enroll the new team member in Disability/Death in Service email Orca `info@orca.ie` with the Name, Date of Birth, Gender, Occupation, Salary of the new team member. Ensure this personally identifiable data is password protected. 

</details>

<details>

<summary>Legal</summary>

1. [ ] @tnix Review team member responses to home working checklist and follow up, if necessary.
</details>
