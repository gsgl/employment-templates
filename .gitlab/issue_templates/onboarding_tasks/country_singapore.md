### For team members located in Singapore

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Download the following documents from [Egnyte](https://globalupside.egnyte.com/app/index.do#storage/files/1/Shared/Clients/GU/Gitlab/Onboarding%20Documents) (Global Upside's online portal):
    - Employee Personal Particulars Form
    - Bank Details Form
    - Consent form 
    - Id/social security cert (front & back)
    - Bank letter
1. [ ] People Experience: Check all required payroll documents have been sent, completed and uploaded to BambooHR.
1. [ ] People Experience: In the team member's onboarding issue, ping the Non-US Payroll team member `@nprecilla` and `@sszepietowska` letting them know that the Payroll questionnaire is completed and in the Payroll Forms folder. 
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>
