### Day 1 - For New Team Members in the US 

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please be sure to enter your full Social Security Number as your National Identification Number in BambooHR on Day 1. If you do not, we will not be able to add you to payroll until it is entered. Please format the number as `###-##-####`.
1. [ ] New team member: If you have not done so before Day 1, make sure you have completed Section 1 of your I-9 in LawLogix (following instructions sent to your personal email address before your start date). Note - this is critical and must be completed on or before your first day of hire. You must contact People Experience if you have difficulty with this form. Send the name and agent of your Designated Agent for Section 2 to People Ops.
1. [ ] New team member: If you have previously contributed to a 401(k) plan in the current year, during your first week please contact USPayroll@gitlab.com to make them aware. Payroll will need this information to ensure we are complying with the IRS 401(k) contribution guidelines.
1. [ ] New team member: Please review our [Disability Inclusion](https://about.gitlab.com/company/culture/inclusion/#disability-inclusion) policy.
    1. [ ] Please ensure that you make a choice in the `Disability Status` field in the Equal Employment Opportunity section of BambooHR's Job tab of your BambooHR profile. There is a `Prefer Not To Answer` option if you do not wish to answer.
    1. [ ] Please read and review the US Dept of Labor's OFCCP [Voluntary Self-Identification of Disability Form](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding_tasks/documents/503_Voluntary_Self-ID_Disability_Form.pdf).
1. [ ] New team member: Please review our [US Veteran Inclusion](https://about.gitlab.com/company/culture/inclusion/#united-states-veteran-inclusion) policy. 
    1. [ ] Please ensure that you make a choice in the `Protected Veteran Status` field in the Equal Employment Opportunity section of BambooHR's Job tab of your BambooHR profile. There is a `Prefer Not To Answer` option if you do not wish to answer, and also a `Not Applicable` option.
    1. [ ] If applicable, and if you so wish, please check the applicable box(es) if you fall under one (or more) of the protected Veteran statuses. 
1. [ ] New team member: Review the [Labor and Employment Notices](https://about.gitlab.com/handbook/labor-and-employment-notices/) handbook page. Be sure to review all US Federal notices and notices under your state of residence. Checking this checkbox is your confirmation of acknowledgement that you have read, reviewed and understood these documents.
1. [ ] New team member: Please review our [People Policies - GitLab Inc (USA)](https://about.gitlab.com/handbook/people-policies/inc-usa/) page.
1. [ ] New team member: Review [Health and Safety Onboarding Checklist](https://docs.google.com/document/d/1DqbNBL6QIFBQlxqi7rWQiodFt6KvSp5fml3llu94Byw/edit?usp=sharing). Check the checkbox to acknowledge that you have read and understand the guidelines for setting up your home office and have or will purchase equipment that adhere to the guidelines.
1. [ ] New team member (California residents only): 
    1. [ ] Read and be aware of the [California Victims of Domestic Violence Leave Notice](https://www.dir.ca.gov/dlse/Victims_of_Domestic_Violence_Leave_Notice.pdf?utm_campaign=17-Q3-AUG-ACC-PAS-SOI-CLIENT-CA%20DOMESTIC%20VIOLENCE%20EMPLOYMENT%20LEAVE%20ACT&utm_medium=email&utm_source=Eloqua&elqTrackId=d6bf3067231c4a75a25cfd2b11703199&elq=164aa89072ad408db913259dae224863&elqaid=10312&elqat=1&elqCampaignId=). Checking this checkbox is your confirmation of acknowledgment that you have read, reviewed, and understood this document.
    1. [ ] Read and be aware of the [California Department of Fair Employment and Housing Sexual Harassment Poster](https://drive.google.com/file/d/10-dnH-M8v4LSqBWhu4bVYk_qwvsqQtyd/view). Checking this checkbox is your confirmation of acknowledgement that you have read, reviewed and understood this document.
    1. [ ] Read and be aware of the [California Department of Fair Employment and Housing Discrimination & Harassment Poster](https://drive.google.com/file/d/1MgIErp7DX4_MwlnL4YvCGFADINWkvNFs/view). Checking this checkbox is your confirmation of acknowledgment that you have read, reviewed, and understood this document.
    1. [ ] Read and be aware of the [California Required Policy document](https://docs.google.com/document/d/1n6tOlhAOJ-wtVjV8Iq-nGa5XgiDfolbHSqmmyE-XQas/edit). Checking this checkbox is your confirmation of acknowledgment that you have read, reviewed, and understood this document.

**To Do after I-9 is complete:**
1. [ ] New team member: Your invitation to ADP will arrive between Day 4-7 as new GitLab team members are frequently added to ADP in waves coinciding with the ending of each pay period. Once you receive the login from ADP to your GitLab email address, update all information in ADP including Direct Deposit, bank details, W4 Withholdings, marital status, etc. Payroll will reach out to you prior to your first paycheck only if all details are updated in ADP. 
1. [ ] New team member: Your invitation to PlanSource will arrive between Day 4-7. Once you receive your invitation, elect your benefits through the PlanSource platform. Your benefit choices will be retroactive to your start date, but [it may take a few weeks to receive ID cards and policy details](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#id-cards). If you have questions, please contact the Willis Towers Watson Benefits Support Helpline at 877-409-9288 or GitLab@willistowerswatson.com. If your questions are sensitive, please contact the [People Connect Team](https://about.gitlab.com/handbook/people-group/people-connect/) via Direct Message in Slack.
    1. [ ] New team member: Please ensure you also elect in to your 2022 benefit plans in addition to your 2021 benefit plans, as GitLab's open enrollment for the 2022 year started on Nov 8, 2021. You should be prompted to start your 2022 Annual Enrollment in PlanSource after you finish your New Hire Enrollment. 
    1. [ ] New team member: For more information on our 2021 and 2022 plans, please review the [handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/) and [Summary Plan Description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing).
1. [ ] New team member: Review [GitLab's 401(k)](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan) Plan. You will receive an email from Betterment within your first week, usually between Day 4-7. If you do not, please reach out to [People Connect](https://about.gitlab.com/handbook/people-group/people-connect/). 
   1. New team member: For more information on the plan, please review the [Summary Plan Description](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing).

</details>

<details>
<summary>People Experience</summary>

### Before Start Date
1. [ ] People Experience: The sync will send the employee email instructions in LawLogix - 7 days prior to the start date. 
1. [ ] People Experience: Lawlogix will automatically send the invite to the new team member's designated Section 2 Agent.
1. [ ] People Experience: Run the completed I-9 through e-verify within the LawLogix Guardian platform. 
1. [ ] People Experience: After the case has been submitted to e-verify, check to see whether employment has been authorized or whether any further action is needed. Steps for photo match can be found in the [Handbook](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#e-verify-photo-matching).
1. [ ] People Experience: Once employment has been authorized, download and save the I-9 E-Verify confirmation form to the `Verification` folder in BambooHR, along with the supporting identity documents. 
1. [ ] People Experience: If this team member is a rehire, immediately ping the Total Rewards for resync of benefits.

### After Start Date
1. [ ] People Experience: Once the I9 is complete and audited, and the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the I9 process is completed and that the employment is authorized. Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting` 
1. [ ] People Experience: Send an email to Finance to let them know that an hourly employee has started.
 
**To Do after I-9 is complete:**
1. [ ] People Experience: Once the BambooHR profile has been audited, check the "I-9 Processed" box located on the Personal tab in BambooHR and add the relevant [Benefit Group](https://docs.google.com/spreadsheets/d/1QU2rsFrrKSRQIrzWu2eqylK0HrNvt9FUhc_S5VQAVJ4/edit?usp=sharing) on the Benefits tab.
1. [ ] People Experience: Comment in the issue with "Ready to add to ADP" to alert US Payroll to add team member to ADP. 

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll @vlaughlan, @ybasha: Add new team member to ADP and send invitation to team member. 
1. [ ] Payroll @vlaughlan, @ybasha: Communicate with PeopleOps Analyst that team member has been successfully added.

</details>

<details>
<summary>Legal</summary>

1. [ ] Legal @tnix: Confirm that team member has acknowledged receipt/review of health and safety onboarding checklist.

</details>


