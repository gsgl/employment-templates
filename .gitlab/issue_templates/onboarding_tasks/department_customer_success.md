#### Customer Success

<details>
<summary>Manager</summary>

1. [ ] Manager: Notify the new member that they should be invited to a Sales Quick Start Classroom which is their Customer Success onboarding experience. 
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1YF4mtTAFmH1E7HqZASFTitGQUZubd12wsLDhzloEz3I/edit) and [Competition](https://about.gitlab.com/devops-tools/).
1. [ ] New team member: Familiarise yourself with the GitLab Sales Learning Framework listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/).
1. [ ] New team member: Review the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).

</details>


