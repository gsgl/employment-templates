## For Support Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Introduce yourself to your team!
   1. [ ] Write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support in the [Support Week in Review doc](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit) before Friday.
   1. [ ] Now, also format and send the introduction post you just created to the `#support_team-chat` Slack channel. Welcome to
      [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!
1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Once you get [ZenDesk](https://gitlab.zendesk.com/agent/dashboard) access, complete the following.
    1. [ ] Add a profile picture to your Zendesk account
    1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
    1. [ ] Under your profile in Zendesk, it should read 'Support Staff'. If it reads Light Agent, inform your manager.
1. [ ] New team member: [Request access to ChatOps.](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
1. [ ] New team member: Team Calls. To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] New team member: Add the Support Time Off calendar as an [additional calendar in PTO by Roots](https://about.gitlab.com/handbook/support/support-time-off.html#one-time-setup-actions) to ensure any time off you schedule automatically gets added to the team calendar as well as your own.
1. [ ] New team member: Learn about [Support's key Slack channels](https://about.gitlab.com/handbook/support/#slack) and join them. If you have questions, discuss with your manager.
1. [ ] New team member: Read about [Support's onboarding learning pathway](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway) to understand the different modules you'll be assigned based on your role.
1. [ ] New team member: Get familiar with the [Support Team Meta project](https://gitlab.com/gitlab-com/support/support-team-meta), the issue tracker for all discussions around all things support.
   1. [ ] Consider configuring [Custom Notification settings](https://docs.gitlab.com/ee/user/profile/notifications.html#change-level-of-project-notifications) for this project, to be notified on all newly created issues. This way you'll be aware of new discussions when they're started.
   **Note:** You'll still need to enable Notifications for specific issues you want to follow to be notified of further updates.



##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] If you would like to make any improvements to this **For Support Only** section, a good exercise in GitLab/git would be to make a merge request to change the [Support onboarding template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding_tasks/department_customer_support.md).
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: At the beginning of the new team member's second week, open the [New Support Team Member Start Here](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=New%20Support%20Team%20Member%20Start%20Here) issue and provide the link in a comment below this onboarding checklist.
1. [ ] Manager: If the new team member will be a ZenDesk Admin, make sure to include that in the access request.
1. [ ] Manager: If the new team member is going to be working US Federal tickets (U.S. Citizens only), open a [Individual or Bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to have them added as an agent in the Zendesk US Federal Instance and assign it to `@jcolyer`.
1. [ ] Manager: The new member will be added to the following calendars with inclusion in `GSuite supportgroup` as part of their standard entitlments.  If needed sooner than that issue being provisioned, you can [manually add new team member](https://support.google.com/calendar/answer/37082?hl=en#:~:text=Click%20More-,Settings%20and%20sharing.,Click%20Send.) to calendars.
   1. [ ] GitLab Support
       1. [ ] Ensure that the new team member is included in the regional call.
   1. [ ] Support Time Off
1. [ ] Manager: Add new team member as an `Owner` to the GitLab.com testing groups:
   1. [ ] [GitLab.com Gold](https://gitlab.com/gitlab-gold/-/group_members)
   1. [ ] [GitLab.com Bronze](https://gitlab.com/gitlab-bronze/-/group_members)
   1. [ ] [GitLab.com Silver](https://gitlab.com/gitlab-silver/-/group_members)
1. [ ] Manager: Help your new team member to introduce themself
   1. [ ] Talk to them about doing an introduction at the next regional team meeting, and add it to the agenda.
</details>

<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the docs on being a [Support Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html),
   which builds on the general [GitLab onboarding buddy docs](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/).
</details>
