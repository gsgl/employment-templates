#### Sales Division

<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to Sales Quick Start Learning Path in EdCast. New sales team member will receive an email prompting them to login to EdCast to begin working through the Sales Quick Start Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any).
Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Send a Slack to Sales Analytics (@Swethakashyap) to have the new team member's quota information uploaded into Salesforce.

</details>

<details>
<summary>Sales Strategy</summary>

1. [ ] Sales Strategy (@Swethakashyap): Set BCR, SCR (if applicable), and prorated quota and deliver participant plan/schedule
1. [ ] Sales Strategy (@Swethakashyap): Deliver Participant Schedule

</details>

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Complete your Sales Quick Start Learning Path (see your email for an invitation from Google Classroom).
If you have questions, please reach out to Tanuja (@tparuchuri).
1. [ ] New team member: Follow [these instructions](https://docs.oracle.com/en/cloud/saas/datafox/dfudf/set-up-and-administer-salesforce-integration.html#sfdcAdminsIntgInstConnectDFSF) to link your DataFox Account with your Salesforce Account.
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep, please consult with your manager to determine whether or not you need access to Zendesk Light Agent.
If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access.
Note: PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above.
PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

</details>

<details>
<summary>Sales Ops</summary>

1. [ ] Marketing (@tav_scott): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

</details>
