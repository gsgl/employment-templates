#### Developer Evangelism

<details>
<summary>New Team Member</summary>

1. [ ] Join the following slack channels next to the [key Slack channels](https://about.gitlab.com/handbook/communication/#key-slack-channels):
    1. [#marketing](https://gitlab.slack.com/archives/C0AKZRSQ5) 
    1. [#corp-mktg](https://gitlab.slack.com/archives/CHLPSUVU7)
    1. [#developer-evangelism](https://gitlab.slack.com/archives/CMELFQS4B)
    1. [#external-comms](https://gitlab.slack.com/archives/CB274TZRR)
    1. [#newswire](https://gitlab.slack.com/archives/CERAPFN7R)
    1. [#cfp](https://gitlab.slack.com/archives/C106ACT6C)
    1. [#alliances](https://gitlab.slack.com/archives/CBMQE38E5)
    1. [#competition](https://gitlab.slack.com/archives/C1BBL1V3K)
1. [ ] Join the [e-group Slack channels](https://about.gitlab.com/handbook/communication/#getting-in-touch-with-the-e-group)
1. [ ] Create [sidebar sections in Slack](https://about.gitlab.com/handbook/communication/#organizing-your-slack-sidebar-by-priority) and organize channels and sections by priority
    1. Example: `info`, `Dev Evangelism`, `team`, `insights`, `product`, etc. 
1. [ ] Read the Marketing Handbook [Page](https://about.gitlab.com/handbook/marketing/).
1. [ ] Review the Marketing [OKRs](https://about.gitlab.com/handbook/marketing/#okrs).
1. [ ] Read the Developer Evangelism Handbook [Page](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/)
    1. Familiarize yourself with CfPs, community tasks, content creation, etc.
1. [ ] Attend the [Group Conversations](https://about.gitlab.com/handbook/people-group/group-conversations/) to learn more about the different teams, responsibilities and visions.
1. [ ] Schedule coffee chats with product managers and engineers to learn more about the product stages and lifecycle.
1. [ ] Setup your [social media accounts](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/social-media/) and update your bio/CV.
    1. Consider creating your own website/blog. Add `About` and `Talks` to link that from CfP submissions as SSoT.
1. [ ] Inspect the [Developer Evangelism GDrive share](https://drive.google.com/drive/folders/0AEUOlCStMBC9Uk9PVA) for additional resources (slide decks, templates, etc.)
1. [ ] Familiarize yourself with [Release Evangelism](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/social-media/#monthly-releases).
1. [ ] Complete the [GitLab external speaker training](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/#external-speaking-trainings)
    1. [ ] [GitLab Media Training](https://gitlab.edcast.com/pathways/ECL-28e87cae-217c-4a23-a9ca-e15e23988096)
    1. [ ] [GitLab Speaker Training](https://gitlab.edcast.com/pathways/gitlab-speaker-training)
</details>


<details>
<summary>Manager</summary>

1. [ ] Verify access to the [GDrive share](https://drive.google.com/drive/folders/0AEUOlCStMBC9Uk9PVA).
1. [ ] Verify access to [development cloud resources](https://about.gitlab.com/handbook/engineering/#resources). 
1. [ ] Verify access to the [Marketing vault in 1Passsword](https://about.gitlab.com/handbook/security/#1password-guide).

</details>

