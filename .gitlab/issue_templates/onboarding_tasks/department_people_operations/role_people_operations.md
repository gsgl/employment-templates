## For People Operations Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to the following private Slack channels (as applicable):
   - `#people-group-confidential`
   - `#people_exp_ops`
   - `#people-exp_ces`
   - `#pbp-peopleops`
   - `#people-ops-tech`
   - `#payroll-peopleops_terms`
   - `#peopleops_totalrewards`
   - `#payroll-peopleops`
   - `#international-exp`
   - `#employment-surveys`
   - `#offboardings`

1. [ ] Manager: Open Operations Training issue for new team member
1. [ ] Manager: Add team member to the following public Slack channels (as applicable):
   - `#peopleops`
   - `#peopleops-alerts`
   - `#people-exp-ops`
   - `#people-group`
   - `#pea-team`
1. [ ] Manager: Add team member to the following Slack groups (as applicable):
   - `peopleops_spec`

</details>

<details>
<summary>People Ops/Experience</summary>

1. [ ] People Ops: @ewegscheider to add team member to Greenhouse as "Job Admin: People Success".
1. [ ] People Ops: If the new team member is on the Compensation Team or an [Executive](https://about.gitlab.com/company/team/structure/#executives), please notify @ewegscheider using the [Greenhouse Approvals Change Request](https://gitlab.com/gl-recruiting/operations/issues/new#) Issue template.
1. [ ] People Experience: Share the People Experience / Ops Shared [Drive](https://drive.google.com/drive/folders/0ABRi3YnroHzzUk9PVA) with the team member.
1. [ ] People Experience: Invite the team member to the PEA Team Calendar in Google Calendar.

</details>

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Review the [People Experience Team Handbook Page](https://about.gitlab.com/handbook/people-group/people-experience-team/).
1. You will be added to the email alias for your team. This can result in a high volume inbox, consider reviewing how your fellow team members are responding to the queries we as a team receive. No need to respond to any of these queries during week 1 or 2.
1. [ ] New Team Member: Review [key Slack channels](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/Key_Slack_Channels.md) for the People Experience team to familiarze yourself with important channels and their purpose.

</details>
