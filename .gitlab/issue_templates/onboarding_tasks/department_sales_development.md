#### Sales Development


<details>
<summary>Manager</summary>

1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to inform Marketing Operations of the territory that the new team member will assume and when lead routing should be started. 
1. [ ] Manager: Send new team member the SDR Participant plan.
1. [ ] Manager: Assign Sales Development Solutions Architect.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: Log into the [SDR channel](https://gitlab.edcast.com/channel/sdr) and focus on completing "Tools Onboarding" during the suggested timeframe. This will teach you about your day-to-date responsibilities as a sales development representative. You should click the three dots at the top right of each week and assign that content to yourself along with start and due dates so that it shows up under "My learning plan".
1. [ ] New team member: Listen to [ten outbound calls in chorus.ai](https://chorus.ai/playlists/147571).
1. [ ] New team member: Listen to eight [initial qualifying meetings (IQMs) in chorus.ai](https://chorus.ai/playlists/280863).
1. [ ] New team member: Once you have received your email invite to DataFox, log in and follow [these instructions](https://help.datafox.com/hc/en-us/articles/227081328-User-Setup-Connect-your-DataFox-User-to-your-Salesforce-Account) to link your DataFox account with your Salesforce account. Tool access emails will be sent to you over the course of your first two weeks.
1. [ ] New SDR team member: Download Google Chrome and set it as your default browser. Google Chrome is the only browser compatible with Outreach.
1. [ ] New team member: Please [add your @gitlab.com email address as an alternative](https://www.linkedin.com/help/linkedin/answer/60/adding-or-changing-your-email-address-for-your-linkedin-account?lang=en) on your LinkedIn profile. This will allow you to easily access LinkedIn Sales Navigator once you have received the email invite.
1. [ ] New team member: Connect ZoomInfo to Salesforce: Once you receive access to ZoomInfo, log in > click your name in the top right hand corner > click 'Account Settings' > at the top left click 'CRM Settings' > ensure current CRM = Salesforce and Connect to = production instance > click authorize.
1. [ ] New team member: Complete TT100, TT101, TT102, and TT110 in [EdCast](https://gitlab.edcast.com/journey/tanuki-tech-level-tanuki). (In the event that you have these sessions scheduled on your calendar, you will be doing these sessions live as opposed to watching recordings.)
1. [ ] New team member: Schedule a coffee chat with our sales development solutions architect.
1. [ ] New team member: Skip through our [SDR Handbook page](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/).
1. [ ] New team member: In your remaining time, start your Sales Quickstart Learning Path (see your email for an invitation from Google Classroom). This training was created specifically for our sales team but it is helpful for you to understand our sales motion, the sales workflow and product information. You will run through SDR specific training throughout your onboarding experience with SDR leadership and your team. Please direct SDR specific questions to your manager. If you have questions about SQS, reach out to John Blevins.

</details>


<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to Sales Quick Start Learning Path in EdCast. New sales team member will receive an email prompting them to login to EdCast to begin working through the Sales Quick Start Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>
