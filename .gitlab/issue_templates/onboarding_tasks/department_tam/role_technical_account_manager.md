#### Technical Account Managers

<details>
<summary>New Team Member</summary>

1. [ ] Read through the [TAM handbook](https://about.gitlab.com/handbook/customer-success/tam/) and its subpages.
1. [ ] Read about how we structure [account teams](https://about.gitlab.com/handbook/customer-success/account-team/).
1. [ ] Read about how we manage [SA/TAM overlap](https://about.gitlab.com/handbook/customer-success/#overlap-between-solution-architects-and-technical-account-managers).
1. [ ] Read about [how to engage with the Product team](https://about.gitlab.com/handbook/product/how-to-engage/).
1. [ ] Request a [Light Agent account in Zendesk](https://about.gitlab.com/handbook/support/internal-support/#viewing-support-tickets).
1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/company/team/?department=customer-support) (appropriate to timezone) on 3-5 calls.
1. [ ] Shadow fellow Technical Account Managers on 4 customer calls.
1. [ ] Bookmark the repo of TAM-supported [SaaS customers](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts/blob/master/marquee-accounts.yml), as you will need to update it if you manage any SaaS customers to ensure you receive appropriate notifications about their subscription.
1. [ ] Learn about environment information and [maintenance checks](http://docs.gitlab.com/ce/administration/raketasks/maintenance.html).
1. [ ] Learn about the [GitLab check](http://docs.gitlab.com/ce/administration/raketasks/check.html) rake command.
1. [ ] Learn about GitLab Omnibus commands ([`gitlab-ctl`](https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-installations)).
   1. [ ] [GitLab Status](https://docs.gitlab.com/omnibus/maintenance/#get-service-status).
   1. [ ] [Starting and stopping GitLab services](https://docs.gitlab.com/omnibus/maintenance/#starting-and-stopping).

</details>
