#### Finance Division

<details>
<summary>New Team Member</summary>

1. Take time to learn about the opportunites available here at GitLab to participate in departmental internships, Team Memeber Resource Groups (TMRGs), subcommittees and more. Read through the following links to familiarize yourself with our policies and resources for developing your career.
      - [ ] Career Development [handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#what-is-career-development)
      - [ ] Create a Career Development Plan using [these](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#career-mapping-and-development) handbook resources and share them with your manager in upcoming 1:1 sessions.
      - [ ] Internship for Learning [handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#internship-for-learning)
      - [ ] Visit the [Growth and Development page](https://about.gitlab.com/handbook/finance/growth-and-development/) in the Handbook to review the intiatives of this Finance subcommittee.
<end>

2. [ ] The Finance division has developed a mentorship program. If you are interested in being a mentor or a mentee, please read through the [handbook page](https://about.gitlab.com/handbook/finance/growth-and-development/mentorship/) and you can sign up at the following links to be a [mentee](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/4047#mentee-signup) and/or to be a [mentor](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/4047).

3. [ ] The finance division uses Simpli5 assessments to increase effective communication among our team members. Simpli5 is a human performance system for increasing productivity in global work environments. It helps individuals, teams, departments and enterprises to acknowledge their strengths, and focuses organizations around a culture of performance and engagement.  Each person in the finance team is able to compare their results against one another which helps us learn how best to communicate with our team mates. Sign up [here](https://docs.google.com/spreadsheets/d/1XXZavfMH3NvZrjG-pr4N91ogcR63k-HefNKl4mtKGP4/edit#gid=0) to complete your Simpli5 assessment to learn about your personality traits.

4. Set up coffee chats with the following "Must Meet" team members that you will be working closely with in your new role. We recommend that you set these up during weeks 2 to 5.
      - [ ] 1. 
      - [ ] 2. 
      - [ ] 3. 
      - [ ] 4. 
      - [ ] 5. 
<end>

5. [ ] Review the [list](https://docs.google.com/document/d/16aZNiJQZRlVHGVJDCBQF9hbBfjWE8rI4Qi7WUiVxe38/edit) of Commonly Used Terms for the Finance division.

6. [ ] Review the [list](https://docs.google.com/document/d/1Cq1Ey2lr0Mga6HXVWxhjBe5w_CLRxt9MX-HIxoGiOjA/edit?usp=sharing) of Finance specific LinkedIn Learning courses that you can take to boost your career in a variety of areas. 

7. [ ] Make sure to join the [#finance-lounge](https://gitlab.slack.com/archives/C01T4RFPYJG) on Slack to get to know the team. 

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager to add 5 (or more) "Must Meet" team members to the list of task number 4 of the "New Team Memeber" section above. These are for coffee chats that the new team member should set up in their first 15 days to help them build relationships with team members that they will interact with frequently.
    Please consider that these should be team members in finance and/or other departments that the new IC will work closely with and will need to quickly build relationships with.

</details>

