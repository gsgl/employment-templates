## For employees or contractors with Safeguard only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: you should have been contacted already by SafeGuard to walk you through their onboarding for payroll. Please reach out to People Operations if you have not received any information or have not been contacted by SafeGuard.
1. [ ] New team member: Read through the [benefits section](https://about.gitlab.com/handbook/benefits/#SafeGuard) for your location.

</details>

<details>

<summary>People Experience</summary>

1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the appropriate People Experience Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>
