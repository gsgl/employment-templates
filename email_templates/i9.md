Subject: URGENT: GitLab I-9 Process Instructions

Hi PERSON,

We are so excited you are joining GitLab!  In order for you to be authorized to work and entered into our payroll, we must have a completed and verified I-9 form. This process is a little different for GitLab, being a 100% remote company. 

Section 1 must be completed before your start day.  Section 2 may be completed up to 2 days after starting, but please do not procrastinate, the sooner the better!

First, complete Section 1 of the I-9 form in the Fragomen portal by creating an account using the following link: https://gitlab.i9servicecenter.com
Create an account with a username and password of your choosing. As a reminder, the forms acceptable for an I-9 can be viewed here. They cannot be expired.

For Section 2, you will need to choose a designated agent to verify your I-9 document(s) in person. This person needs to be a US Citizen, and 18 and over. They cannot be a notary. The agent can be a family member, friend, former co-worker, neighbor, etc.

You will need to arrange a time to meet with them in person. Once you have completed Section 1, please complete this form: https://forms.gle/6yxpFHVvwQ2LBQBf6 which will let us know the name and email of your agent.

Every afternoon, we check the form responses and your designated agent will receive instructions to complete Section 2 with you there in person so that they may verify your documents in person. Please do not meet up with the agent before receiving confirmation from them that they have received their instructions. Your agent will also need photos or scans of your documents to upload to the Fragomen portal when prompted. When the system asks the agent to enter their "Title", this is their current work title, i.e. "Senior Project Manager", etc..

This entire process should take less than 15 minutes. Please let us know if you have any questions! If you run into any technical issues with logging in or signing the online document, you must call the I-9 Service Center directly at (415) 263-8459 or (408) 330-1110. The People Experience team is not able to support and answer any questions regarding the I-9 portal as it is maintained by Fragomen in accordance with USCIS laws.

Once the I-9 has been completed, it will be run through E-Verify. To learn more about E-verify, you can visit the website [here](https://www.e-verify.gov/employees). Please do let us know if you have any questions regarding this process. 
