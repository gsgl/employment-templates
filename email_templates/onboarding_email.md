Hi `__FIRST_NAME__`,

Welcome to GitLab! We're so excited that you've joined our team!

To ensure the seamless setup of your work accounts, please log out from any personal Gsuite/Gmail and GitLab.com accounts before beginning any of the below steps.

<b>Okta</b> is our identity and authentication portal, which you will use to access most of your cloud-based applications. You should have received an invitation email from GitLab about Okta, and please follow the instructions in that email to activate your account. For further information about how GitLab uses Okta,<a href='https://about.gitlab.com/handbook/business-technology/okta'>please check out the handbook page</a>.
(If you can't find the Okta activation email, please firstly check your Spam/Junk Folders or other folders in case your email client has filed it somewhere. The IT Ops team can assist if you are unable to find it or if the token has expired. Please email itops@gitlab.com, and cc people-exp@gitlab.com for visibility.)

To be as helpful as possible, we created the following <a href='https://www.youtube.com/watch?v=Y5lev17qXbw&feature=youtu.be'>video</a> to help you understand how to start your onboarding process. Please make sure to create a new GitLab.com account dedicated for your work at GitLab. It's important to keep this new account separate from any personal projects or engagements as this new account will be disabled on leaving GitLab. <b>Please ensure not to use Okta or GoogleSSO to create a GitLab account</b>, the account will not have the correct permission level to get into your onboarding issue. 

Once you have completed the quick tasks listed in the video, you will be ready to access your <a href='`__GITLAB_ISSUE_LINK__`'>Onboarding Issue</a>. Please be sure to complete all the <b>Day 1 tasks</b> assigned to you (i.e., "New Team Member") in your Onboarding Issue today, your first day, as they are critical to adding you to payroll and benefits.

Please be aware, At GitLab, we're prolific at documenting what we do in our handbook, the website, and in GitLab documentation. This may make it difficult to find specific pieces of content and find answers to questions. As you go through your first day, week, and so on, it will be helpful to read the <a href='https://about.gitlab.com/handbook/tools-and-tips/searching/'>Searching the GitLab website like a Pro</a> page to better understand how to search our documentation to find answers to your questions.

Regarding your <b>GitLab Email</b> reset message, be sure to check your Spam folder to make sure it is not there! To access your GitLab email, we recommend that you right-click on the "Reset password" button in your Reset email, Copy Link Address, open an Incognito window in your browser and paste the link. Then you will be asked to Create password -> Add account (your new GitLab email) -> Log in with the password you just created.

Thank you and congratulations again for joining the team!

All the best,

`__PEOPLE_EXPERIENCE_NAME__`
